<h1 align="center">三方组件资源汇总</h1> 

##### 本文收集了一些已经开源的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者贡献自己的开源组件库，可以提PR加入到列表当中 


## 目录

- [工具](#工具)
- [三方组件](#三方组件)
  - [工具类](#工具类)
    - [图片加载](#图片加载)
    - [数据封装传递](#数据封装传递)
    - [日志](#日志)
    - [权限相关](#权限相关)
    - [相机-相册](#相机-相册)
    - [其他工具类](#其他工具类)
  - [网络类](#网络类)
    - [网络类](#网络类)
  - [文件数据类](#文件数据类)
    - [数据库](#数据库)
    - [Preferences](#preferences)
    - [数据存储](#数据存储)
  - [UI-自定义控件](#ui-自定义控件)
    - [Image](#image)
    - [Text](#text)
    - [Button](#button)
    - [ListContainer](#listcontainer)
    - [PageSlider](#pageslider)
    - [ProgressBar](#progressbar)
    - [Dialog-弹出框](#dialog-弹出框)
    - [Layout](#layout)
    - [Tab-菜单切换](#tab-菜单切换)
    - [Time-Date](#time-date)
    - [其他UI-自定义控件](#其他ui-自定义控件)
  - [框架类](#框架类)
    - [框架类](#框架类)
  - [动画图形类](#动画图形类)
    - [动画](#动画)
    - [图片处理](#图片处理)
  - [音视频](#音视频)
  - [游戏](#游戏)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio
- [HAPM官网地址](https://hpm.harmonyos.com/hapm/#/cn/home) - [HAPM介绍](https://hpm.harmonyos.com/hapm/#/cn/help/introduction)

[返回目录](#目录)

## 三方组件

### 工具类

#### 图片加载

- [glide](https://gitee.com/openharmony-tpc/glide) - 最常用的图片加载工具
- [glide-transformations](https://gitee.com/openharmony-tpc/glide-transformations) - 基于glide 的图片变化库
- [fresco](https://gitee.com/openharmony-tpc/fresco) - facebook出品的一款图片加载工具
- [picasso](https://gitee.com/openharmony-tpc/picasso) - 常用的图片加载工具之一
- [ohos-gif-drawable](https://gitee.com/openharmony-tpc/ohos-gif-drawable) - gif图片加载工具
- [Keyframes](https://gitee.com/openharmony-tpc/Keyframes) - gif图片加载工具
- [ion](https://gitee.com/openharmony-tpc/ion) - 图片加载工具
- [coil](https://gitee.com/baijuncheng-open-source/coil) - 一款用于图片加载的库
-  :tw-1f195: [ohos-smart-image-view](https://gitee.com/chinasoft_ohos/ohos-smart-image-view) - ohos-smart-image-view是一个从URL或用户的联系地址簿中加载图像。图像被高速缓存到内存和磁盘，以实现超快速加载
-  :tw-1f195: [Cube-ImageLoader](https://gitee.com/hihopeorg/Cube-ImageLoader) - 这个框架致力于快速实现图片加载需求，解放生产力

[返回目录](#目录)

#### 数据封装传递

- [EventBus](https://gitee.com/openharmony-tpc/EventBus) - 最常用的消息传递工具，发布/订阅事件总线
- [Rxohos](https://gitee.com/openharmony-tpc/Rxohos) - RxJava3的openharmony特定绑定的反应性扩展。该模块向RxJava添加了最小的类，这些类使在openharmony应用程序中编写反应式组件变得容易且轻松。更具体地说，它提供了一个可在主线程或任何给定EventRunner上进行调度的Scheduler
- [RxBus](https://gitee.com/openharmony-tpc/RxBus) - 基于Rxjava消息传递工具
- [otto](https://gitee.com/openharmony-tpc/otto) - 基于Guava的消息传递工具
- [RxLifeCycle](https://gitee.com/openharmony-tpc/RxLifeCycle) - 基于RxJava生命周期获取，此功能很有用，因为不完整的订阅可能会导致内存泄漏
- [RxBinding](https://gitee.com/openharmony-tpc/RxBinding) - 以rxjava的形式来处理ohos中的ui事件
- [agera](https://gitee.com/openharmony-tpc/agera) - Agera 是一组类和接口，用于帮助编写功能性、异步和反应式应用程序
- [Anadea_RxBus](https://gitee.com/openharmony-tpc/Anadea_RxBus) - 支持注解和动态绑定的事件总线框架
- [LoadSir](https://gitee.com/openharmony-tpc/LoadSir) - 注册事件进行回调操作
- [Aria](https://gitee.com/hihopeorg/Aria) - 文件下载上传框架

[返回目录](#目录)

#### 日志

- [Logger](https://gitee.com/openharmony-tpc/logger) - log工具，简单，漂亮，功能强大的记录器
- [xLog](https://gitee.com/openharmony-tpc/xLog) - 日志工具，可同时在多个通道打印日志，如 hilog、Console 和文件。如果你愿意，甚至可以打印到远程服务器（或其他任何地方）
- [KLog](https://gitee.com/chinasoft_ohos/KLog) - HiLog 工具类
- [tinylog](https://gitee.com/archermind-ti/tinylog) - 日志工具
- [Timber_ohos](https://gitee.com/isrc_ohos/timber_ohos) - 基于开源项目Timber进行鸿蒙化的移植和开发，增强鸿蒙输出日志的能力
-  :tw-1f195: [LogUtils](https://gitee.com/chinasoft_ohos/LogUtils) - 日志管理器
-  :tw-1f195: [hyperlog-ohos](https://gitee.com/openneusoft/hyperlog-ohos) - 日志记录工具，并将日志记录在数据库中

[返回目录](#目录)

#### 权限相关

- [XXPermissions](https://gitee.com/openharmony-tpc/XXPermissions) - 权限申请，一键式权限请求框架	
- [PermissionsDispatcher](https://gitee.com/openharmony-tpc/PermissionsDispatcher)	 - 权限申请，提供了一个简单的基于注解的API来处理运行时权限。该库减轻了编写一堆检查语句（无论是否已授予您权限）带来的负担，以保持您的代码干净安全	
- [Dexter](https://gitee.com/openharmony-tpc/Dexter) - 权限申请，简化在运行时请求权限的过程
- [RuntimePermission](https://gitee.com/archermind-ti/RuntimePermission) - 请求运行时权限的最简单方法，不需要扩展类或重写permissionResult方法	
- [permission-helper](https://gitee.com/baijuncheng-open-source/permission-helper) - 权限管理请求库
-  :tw-1f195: [HiPermission](https://gitee.com/chinasoft_ohos/HiPermission) - 一个简单易用的漂亮权限申请库
-  :tw-1f195: [easypermissions](https://gitee.com/chinasoft_ohos/easypermissions) - 动态权限申请
-  :tw-1f195: [EffortlessPermissions](https://gitee.com/archermind-ti/effortless-permissions) - 一个 OpenHarmony 权限库，通过方便的添加扩展了 OpenHarmony 权限
-  :tw-1f195: [RuntimePermission](https://gitee.com/archermind-ti/RuntimePermission) - runtimePermission具有进行权限申请功能，可以对需要的不同权限进行申请
-  :tw-1f195: [soul-permission](https://gitee.com/openneusoft/soul-permission) - 相关权限的存在校验及权限设定提示

[返回目录](#目录)

#### 相机-相册

- [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 基于ZXing的二维码扫描工具	
- [Matisse](https://gitee.com/openharmony-tpc/Matisse) - 选择图库图片	
- [ImagePicker](https://gitee.com/openharmony-tpc/ImagePicker) - 相册访问
- [CameraView](https://gitee.com/openharmony-tpc/CameraView) - 相机使用组件
- [easyqrlibrary](https://gitee.com/archermind-ti/easyqrlibrary) - 二维码扫描器
- [zxing-embedded](https://gitee.com/baijuncheng-open-source/zxing-embedded) - 基于ZXING，二维码条形码扫描库
- [qrcode-reader-view](https://gitee.com/baijuncheng-open-source/qrcode-reader-view) - 一个简易的相机扫码工具
- [barcodescanner](https://gitee.com/baijuncheng-open-source/barcodescanner) - 基于zxing和zbar提供易于使用的二维码扫描功能	
- [certificate-camera](https://gitee.com/baijuncheng-open-source/certificate-camera) - 一个拍摄证件照片的相机工具。	
- [Zbar_ohos](https://gitee.com/isrc_ohos/ZBar_ohos) - 基于开源项目Zbar进行鸿蒙化的移植和开发，条形码阅读	
-  :tw-1f195: [ImageSelector](https://gitee.com/chinasoft2_ohos/ImageSelector) - 一个功能强大的图片选择器
-  :tw-1f195: [zBarLibary](https://gitee.com/chinasoft2_ohos/zBarLibary) - zxing二维码生成、识别
-  :tw-1f195: [QRCodeScanner](https://gitee.com/chinasoft_ohos/QRCodeScanner) - 二维码扫描器
-  :tw-1f195: [CameraFragment](https://gitee.com/chinasoft_ohos/CameraFragment) - 一个简单的易于集成的相机Fragment
-  :tw-1f195: [PixImagePicker](https://gitee.com/chinasoft3_ohos/PixImagePicker) - PixImagePicker是一个拍照、录像，照片和视频选择功能库
-  :tw-1f195: [ContentManager](https://gitee.com/archermind-ti/content-manager) - 本库用于从图库、相机等设备获取图片、视频
-  :tw-1f195: [MagicalCamera](https://gitee.com/archermind-ti/magical-camera) - 在 OpenHarmony 中拍照和选择图片的魔法库。 方法很简单，如果需要也可以把图片保存在设备中，获取真实的uri路径或者图片或者获取图片的私密信息
-  :tw-1f195: [imagepicker](https://gitee.com/openneusoft/imagepicker) - 设备上获取照片（从相册、文件中选择）、压缩图片的开源工具库
-  :tw-1f195: [code-scanner](https://gitee.com/openneusoft/code-scanner) - 实现了相机的一些功能和条形码扫描的功能
-  :tw-1f195: [LongImageCamera](https://gitee.com/openneusoft/long-image-camera) - 通过相机视图捕获多图像进行合并拼接，形成长图像并进行预览或手势操作
-  :tw-1f195: [titan-camera](https://gitee.com/openneusoft/titan-camera) - 实现一个应用内置相机，可以处理预览大小，预览拉伸

[返回目录](#目录)

#### 其他工具类

- [Butterknife](https://gitee.com/openharmony-tpc/butterknife) - 通过反射调用方法，使用注解处理为您生成样板代码	
- [assertj-ohos](https://gitee.com/openharmony-tpc/assertj-ohos) - 一组旨在测试ohos的断言库
- [ohos-utilset](https://gitee.com/openharmony-tpc/ohos-utilset) - 工具集
- [xUtils3](https://gitee.com/openharmony-tpc/xUtils3) - 包含了orm,http（s）,image, Component注解的工具集合，特性强大，方便拓展	
- [device-year-class](https://gitee.com/openharmony-tpc/device-year-class) - 获取手机年份
- [swipe](https://gitee.com/openharmony-tpc/swipe) - 对于手势封装应用
- [TinyPinyin](https://gitee.com/openharmony-tpc/TinyPinyin) - 低内存占用的汉字转拼音工具库
- [ohos-bluetooth-kit](https://gitee.com/hihopeorg/ohos-bluetooth-kit) - 蓝牙设备通信
- [ohos-IMSI-Catcher-Detector](https://gitee.com/hihopeorg/ohos-IMSI-Catcher-Detector) - IMSI探测器
- [Battery_Metrics](https://gitee.com/hihopeorg/Battery-Metrics) - 检测电池相关系统指标的库
- [CheckVersionLib](https://gitee.com/hihopeorg/CheckVersionLib) - 版本检测更新库
- [ErrorProne](https://gitee.com/hihopeorg/error-prone) - 将常见的Java语法错误捕获为编译错误显示出来	
- [FastBle](https://gitee.com/hihopeorg/FastBle) - 蓝牙设备通信
- [RxOhosBle](https://gitee.com/hihopeorg/rxohosble) - 蓝牙设备通信
-  :tw-1f195: [ohos-BLE](https://gitee.com/hihopeorg/ohos-BLE) - 蓝牙框架,提供了扫描、连接、使能/除能通知、发送/读取数据、接收数据,读取rssi,
-  :tw-1f195: [RxTool](https://gitee.com/baijuncheng-open-source/RxTool) - 工具类合集
- [truth](https://gitee.com/hihopeorg/truth) - 代码断言工具
- [KeyboardVisibilityEvent](https://gitee.com/hihopeorg/KeyboardVisibilityEvent) - 键盘显示隐藏监听工具
- [StatusBarUtil](https://gitee.com/hihopeorg/StatusBarUtil) - 状态栏管理工具
- [Router](https://gitee.com/chinasoft_ohos/Router) - 通过一行url去指定打开指定页面Ability的工具
- [Once](https://gitee.com/chinasoft_ohos/Once) - 提供一个简单的API来跟踪应用程序是否已经在给定的范围内执行了操作
- [libphonenumber-ohos](https://gitee.com/chinasoft_ohos/libphonenumber-ohos) - 电话归属地查询	
- [ohos-gesture-detectors](https://gitee.com/chinasoft_ohos/ohos-gesture-detectors) - 实现各种手势检测功能	
- [Commonmark-java](https://gitee.com/chinasoft_ohos/Commonmark-java) - 自定义表扩展名
- [LocationManager](https://gitee.com/chinasoft_ohos/LocationManager) - 简化用户位置的获取
- [phrase](https://gitee.com/chinasoft_ohos/phrase) - 字符串处理工具
- [JsonLube](https://gitee.com/chinasoft_ohos/JsonLube) - Json高效解析工具
- [Notify-ohos](https://gitee.com/chinasoft_ohos/Notify-ohos) - 一个统一通知管理的功能库	
- [objenesis_ohos](https://gitee.com/archermind-ti/objenesis_ohos) - Objenesis是一个轻量级的Java库，作用是绕过构造器创建一个实例
- [update-checker-lib](https://gitee.com/baijuncheng-open-source/update-checker-lib) - 目前仅酷安网的更新检查检查
- [Parceler_ohos](https://gitee.com/isrc_ohos/parceler_ohos) - 序列化与反序列化封装实现
- [JodaTime_ohos](https://gitee.com/isrc_ohos/joda-time_ohos) - 日期和时间处理库
- [ANR-WatchDog-ohos](https://gitee.com/isrc_ohos/anr-watch-dog-ohos) - 检测ANR错误并引发有意义的异常工具
- [ViewServer_ohos](https://gitee.com/isrc_ohos/view-server_ohos) - 可视化界面显示布局调试支持工具	
- [libyuv](https://gitee.com/openharmony-tpc/libyuv) - 将ARGB图像转换为RGBA
- [ReLinker](https://gitee.com/openharmony-tpc/ReLinker) - native库加载器
- [FastBle](https://gitee.com/openharmony-tpc/FastBle) - 蓝牙快速开发框架
- [LoganSquare](https://gitee.com/openharmony-tpc/LoganSquare) - JSON解析和序列化库
- [CustomActivityOnCrash](https://gitee.com/openharmony-tpc/CustomActivityOnCrash) - 崩溃时启动自定义页面	
- [RxScreenshotDetector](https://gitee.com/openharmony-tpc/RxScreenshotDetector) - 截屏检测器
- [seismic](https://gitee.com/openharmony-tpc/seismic) - 设备抖动检测
- [AutoDispose](https://gitee.com/openharmony-tpc/AutoDispose) - RxJava工具库	
- [webp-ohos](https://gitee.com/openharmony-tpc/webp-ohos) - 节省内存空间的图片形式
- [Encryption](https://gitee.com/archermind-ti/Encryption) - 字符串加密解密工具
- [Ohos-Intent-Library](https://gitee.com/archermind-ti/Ohos-Intent-Library) - Intent跳转封装库
- [Armadillo](https://gitee.com/archermind-ti/armadillo) - 加密Preferences数据
-  :tw-1f195: [java-aes-crypto](https://gitee.com/chinasoft_ohos/java-aes-crypto) - 用于简单加密解密的类
-  :tw-1f195: [TrustKit-ohos](https://gitee.com/chinasoft_ohos/TrustKit-ohos) - 提供在任何Ohos应用程序中轻松部署ssl公钥锁定和报告功能的库
-  :tw-1f195: [ohos-weak-handler](https://gitee.com/chinasoft_ohos/ohos-weak-handler) - 弱引用内存安全的 Handler
-  :tw-1f195: [EasyProtector](https://gitee.com/chinasoft_ohos/EasyProtector) - ohos上提供的安全功能： 1、安全防护 2、检查root 3、检查Xposed 4、反调试 5、应用多开 6、模拟器检测
-  :tw-1f195: [easydeviceinfo](https://gitee.com/chinasoft_ohos/easydeviceinfo) - 方便的获取手机设备的各种数据信息的库
-  :tw-1f195: [countly-sdk-ohos](https://gitee.com/chinasoft2_ohos/countly-sdk-ohos) - 行为日志收集和性能分析
-  :tw-1f195: [duktape-ohos](https://gitee.com/chinasoft_ohos/duktape-ohos) - 用于Duktape嵌入式JavaScript引擎
-  :tw-1f195: [shortbread](https://gitee.com/chinasoft_ohos/shortbread) - 一个通过注解快捷创建shortcut的工具库
-  :tw-1f195: [ohos-multipicker-library](https://gitee.com/chinasoft_ohos/ohos-multipicker-library) - 文件选择工具
-  :tw-1f195: [OhosScreenAdaptation](https://gitee.com/chinasoft_ohos/OhosScreenAdaptation) - 屏幕分辨率适配
-  :tw-1f195: [NettyChat](https://gitee.com/chinasoft_ohos/NettyChat) - 即时聊天功能
-  :tw-1f195: [ActivityRouter](https://gitee.com/chinasoft_ohos/ActivityRouter) - 支持给Ability定义 URL，这样可以通过 URL 跳转到Ability，支持在浏览器以及 app 中跳入
-  :tw-1f195: [matomo-sdk-ohos](https://gitee.com/chinasoft_ohos/matomo-sdk-ohos) - 可以解析每个按钮的点击事件所上报的数据
-  :tw-1f195: [merlin](https://gitee.com/chinasoft_ohos/merlin) - 手机，wifi网络状态监听
-  :tw-1f195: [Recovery](https://gitee.com/chinasoft2_ohos/Recovery) - 捕获应用崩溃框架，并能恢复崩溃页面
-  :tw-1f195: [SensorManager](https://gitee.com/chinasoft2_ohos/SensorManager) - 这个一个关于传感器相关的功能用法，里面包含多种传感器的使用方法以及测试Demo
-  :tw-1f195: [okble](https://gitee.com/chinasoft3_ohos/okble) - 简单易用的BLE library
-  :tw-1f195: [okbinder](https://gitee.com/chinasoft3_ohos/okbinder) - 一个轻量级的跨进程通信方案，可以用来替代 AIDL
-  :tw-1f195: [FileTransfer](https://gitee.com/chinasoft3_ohos/FileTransfer) - FileTransfer web端与app端文件传输
-  :tw-1f195: [Ohos-Scanner-Compat-Library](https://gitee.com/chinasoft2_ohos/Ohos-Scanner-Compat-Library) - 蓝牙的操作库
-  :tw-1f195: [version-compare](https://gitee.com/chinasoft_ohos/version-compare) - 软件版本号比较工具
-  :tw-1f195: [MagicaSakura](https://gitee.com/chinasoft3_ohos/MagicaSakura) - MagicaSakura是一个openharmony多主题库，支持每日色彩主题和夜间主题
-  :tw-1f195: [sensey](https://gitee.com/chinasoft_ohos/sensey) - 传感器封装
-  :tw-1f195: [Bluetooth-LE-Library---ohos](https://gitee.com/chinasoft3_ohos/Bluetooth-LE-Library---ohos) - 该库可轻松访问Bluetooth LE设备的AdRecord和RSSI值。它为iBeacons提供了其他功能。差异点因为openharmony目前暂不支持系统分享原因，通过intent分享功能没有实现
-  :tw-1f195: [GlideBitmapPool](https://gitee.com/chinasoft2_ohos/GlideBitmapPool) - 用于重用位图内存的内存管理库
-  :tw-1f195: [PickiT](https://gitee.com/chinasoft3_ohos/PickiT) - 该库可通过文件的Uri获取到文件的path功能
-  :tw-1f195: [ohos-visualizer](https://gitee.com/chinasoft3_ohos/ohos-visualizer) - 一个显示频谱的控件
-  :tw-1f195: [ChinaMapView](https://gitee.com/chinasoft3_ohos/ChinaMapView) - 实现通过绘制map的方式进行统计，通过着色器来修改地图上各个组件颜色的操作(由于鸿蒙不支持事件分发机制不完善，导致滑动事件冲突未实现)
-  :tw-1f195: [colorpicker](https://gitee.com/chinasoft2_ohos/colorpicker) - 一套新颖好用的颜色选择器，可以通过弹出框的形式显示，可以随意选择颜色并且生成对应的颜色值，自定义圆形按钮，通过选择颜色改变按钮显示效果，多界面显示，可以收拾滑动，显示多个颜色选择器在不同界面
-  :tw-1f195: [librtmp](https://gitee.com/archermind-ti/librtmp) - Librtmp是用于RTMP流的工具包。 支持所有形式的RTMP，包括rtmp://，rtmpt://，rtmpe://，rtmpte://和rtmps://
-  :tw-1f195: [hwcpipe](https://gitee.com/archermind-ti/hwcpipe) - HWCPipe是一个arm平台获取CPU和GPU硬件计数器的项目
-  :tw-1f195: [xCrash](https://gitee.com/archermind-ti/xcrash) - xCrash为 app 提供捕获 java 崩溃，native 崩溃和 ANR 的能力。不需要 root 权限或任何系统权限
-  :tw-1f195: [bugshaker](https://gitee.com/archermind-ti/bugshaker) - BugShaker允许你的QA团队和/或最终用户通过晃动他们的设备来轻松提交bug报告
-  :tw-1f195: [AppUpdate](https://gitee.com/archermind-ti/appupdate) - 一个简单、轻量、可随意定制的OpenHarmony版本更新库
-  :tw-1f195: [LifecycleModel](https://gitee.com/archermind-ti/lifecycle-model) - LifecycleModel 实现了 Fraction 与 Fraction 之间, Ability 与 Fraction 之间的通讯以及共享数据
-  :tw-1f195: [Share2](https://gitee.com/archermind-ti/share2) - Share2 利用了 OpenHarmony 的原生 API 实现了分享功能，支持文本信息、图片、音视频等其他类型文件的分享
-  :tw-1f195: [DroidAssist](https://gitee.com/archermind-ti/DroidAssist) - `DroidAssist` 是一个轻量级的字节码编辑插件，基于 `Javassist` 对字节码操作，根据 xml 配置处理 class 文件，以达到对 class 文件进行动态修改的效果
-  :tw-1f195: [TaskManager](https://gitee.com/archermind-ti/taskmanager) - TaskManager任务管理器
-  :tw-1f195: [NcAppFeedback](https://gitee.com/archermind-ti/NcAppFeedback) - 让用户使用电话电子邮件客户端或匿名使用 SparkPost 电子邮件服务进行反馈
-  :tw-1f195: [stunning-signature](https://gitee.com/openneusoft/stunning-signature) - 防止篡改APK文件的签名库
-  :tw-1f195: [markdown](https://gitee.com/openneusoft/markdown) - 读取Markdown文件,将Markdown格式转换为Html格式
-  :tw-1f195: [AndLinker](https://gitee.com/openneusoft/and-linker) - AndLinker是 IPC (进程间通信) 库，结合了AIDL和Retrofit的诸多特性，且可以与RxJava和RxJava2的Call Adapters无缝结合使用
-  :tw-1f195: [HarmonyOSRate](https://gitee.com/baijuncheng-open-source/harmony-osrate) - 应用评分
-  :tw-1f195: [StatusBarUtil](https://gitee.com/baijuncheng-open-source/statusbarutil) - 状态栏工具类
-  :tw-1f195: [Animewallpaper](https://gitee.com/baijuncheng-open-source/animewallpaper) - 高清动画壁纸
-  :tw-1f195: [FishBun](https://gitee.com/ts_ohos/FishBun) - 主要涉及功能为读取手机图片，并进行选择操作。以及部分关于开发相关的配置。
-  :tw-1f195: [ohosFilePicker](https://gitee.com/ts_ohos/file-picker) - 文件选择器
-  :tw-1f195: [DevUtils](https://gitee.com/ts_ohos/dev_utils) - 封装快捷使用的工具类及 API 方法调用 该项目尽可能的便于开发人员，快捷、高效开发安全可靠的项目。
-  :tw-1f195: [ohos-fest](https://gitee.com/hihopeorg/ohos-fest) - 常见容器、控件、方法类封装对应断言方法，并支持扩展。
-  :tw-1f195: [ohos-Common](https://gitee.com/hihopeorg/ohos-Common) - 该组件为工具类组件，主要包含常用工具类以及下拉刷新等控件
-  :tw-1f195: [PercentSmoothHandler](https://gitee.com/openharmony-tpc/PercentSmoothHandler) - 自定义Handler

[返回目录](#目录)

### 网络类

#### 网络类

- [PersistentCookieJar](https://gitee.com/openharmony-tpc/PersistentCookieJar) - 基于okhttp3实现的cookie网络优化
- [chuck](https://gitee.com/openharmony-tpc/chuck) - okhttp本地client
- [google-http-java-client](https://gitee.com/openharmony-tpc/google-http-java-client) - google http Client库
- [ohos-async-http](https://gitee.com/openharmony-tpc/ohos-async-http) - 基于Apache的HttpClient库构建的Http Client
- [okhttp-OkGo](https://gitee.com/openharmony-tpc/okhttp-OkGo) - 基于okhttp 封装的库
- [ohosAsync](https://gitee.com/openharmony-tpc/ohosAsync) - 异步网络请求
- [Fast-ohos-Networking](https://gitee.com/openharmony-tpc/Fast-ohos-Networking) - 快速访问
- [FileDownloader](https://gitee.com/openharmony-tpc/FileDownloader) - 文件下载库
- [PRDownloader](https://gitee.com/openharmony-tpc/PRDownloader) - 文件下载库
-  :tw-1f195: [ohosDownloader](https://gitee.com/openneusoft/ohosdownloader) - 一个面向ohos的开源多线程和多任务下载框架
- [network-connection-class](https://gitee.com/openharmony-tpc/network-connection-class) - 获取网络状态库
- [ThinDownloadManager](https://gitee.com/openharmony-tpc/ThinDownloadManager) - 文件下载库
- [AndServer](https://gitee.com/hihopeorg/AndServer) - 网络部署与反向代理设置
- [autobahn-java](https://gitee.com/hihopeorg/autobahn-java) - WebSocket协议和Web应用程序消息传递协
- [Smack](https://gitee.com/hihopeorg/Smack) - 用于与XMPP服务器进行通信，以执行实时通信，包括即时消息和群聊
- [RxEasyHttp](https://gitee.com/hihopeorg/RxEasyHttp) - 基于RxJava2+Retrofit2实现简单易用的网络请求框架
- [retrofit-cache](https://gitee.com/archermind-ti/retrofit-cache) - 通过注解配置，可以针对每一个接口灵活配置缓存策略
- [okdownload](https://gitee.com/openharmony-tpc/okdownload) - 下载引擎
- [NoHttp](https://gitee.com/openharmony-tpc/NoHttp) - 实现Http标准协议框架，支持多种缓存模式，底层可动态切换OkHttp,URLConnection
- [ReactiveNetwork](https://gitee.com/openharmony-tpc/ReactiveNetwork) - 监听网络连接状态以及与RxJava Observables的Internet连接
- [okhttputils](https://gitee.com/openharmony-tpc/okhttputils) - okhttp的封装辅助工具
-  :tw-1f195: [okhttp](https://gitee.com/chinasoft_ohos/okhttp) - PUT，DELETE，POST，GET等请求、文件的上传下载、加载图片(内部会图片大小自动压缩)、支持请求回调，直接返回对象、对象集合、支持session的保持
-  :tw-1f195: [BaseOkHttpV3](https://gitee.com/chinasoft_ohos/BaseOkHttpV3) - OkHttp的二次封装库，提供各种快速使用方法以及更为方便的扩展功能。提供更高效的Json请求和解析工具以及文件上传下载封装，HTTPS和Cookie操作也更得心应手
-  :tw-1f195: [OhosNetworkTools](https://gitee.com/chinasoft_ohos/OhosNetworkTools) - 这是一个 networkTools网络工具类，端口扫描，子网设备查找（本地网络上发现设备）
-  :tw-1f195: [safe-java-js-webview-bridge](https://gitee.com/chinasoft2_ohos/safe-java-js-webview-bridge) - 抛弃使用高风险的WebView addJavascriptInterface方法，通过对js层调用函数及回调函数的包装，支持异步回调，方法参数支持js所有已知的类型，包括number、string、boolean、object、function
-  :tw-1f195: [ZWebView](https://gitee.com/chinasoft2_ohos/ZWebView) - 建立移动端和Web的JS桥接框架，实现通过容器WebView实现移动端与js的互调功能
-  :tw-1f195: [HtmlBuilder](https://gitee.com/chinasoft2_ohos/html-builder) - html页面的加载
-  :tw-1f195: [OkSocket](https://gitee.com/chinasoft2_ohos/OkSocket) - 是一款基于Tcp协议的Socket通讯（长连接）
-  :tw-1f195: [StompProtocolOhos](https://gitee.com/chinasoft3_ohos/StompProtocolOhos) - 对STOMP协议支持长连接 收发消息
-  :tw-1f195: [bizsocket](https://gitee.com/chinasoft2_ohos/bizsocket) - 断线重连、一对一请求、通知、粘性通知、串行请求合并、包分片处理(AbstractFragmentRequestQueue)、缓存、拦截器、支持rxjava，提供类似于retrofit的支持、提供rxjava和rxjava2两种使用方式
-  :tw-1f195: [OkHttpFinal](https://gitee.com/hihopeorg/OkHttpFinal) - 一个对OkHttp封装的简单易用型HTTP请求和文件下载管理框架
-  :tw-1f195: [ok2curl](https://gitee.com/openneusoft/ok2curl) - 将OkHttp请求转换为curl日志
-  :tw-1f195: [RetrofitUrlManager](https://gitee.com/archermind-ti/retrofit-url-manager) - 以最简洁的 Api 让 Retrofit 同时支持多个 BaseUrl 以及动态改变 BaseUrl
-  :tw-1f195: [TrebleShot_ohos](https://gitee.com/archermind-ti/treble-shot-ohos) - 通过可用连接，发送和接收文件
-  :tw-1f195: [multi-thread-downloader](https://gitee.com/openneusoft/multi-thread-downloader) - 轻量级支持断点续传的多线程下载器
-  :tw-1f195: [RxRetroJsoup](https://gitee.com/openneusoft/rx-retro-jsoup) - 响应式的请求网络框架
-  :tw-1f195: [RxWebSocket](https://gitee.com/openneusoft/rx-web-socket) - 基于okhttp和RxJava封装的WebSocket客户端
-  :tw-1f195: [Kalle](https://gitee.com/openneusoft/kalle) - HttpClient，遵循Http标准协议，支持同步请求和异步请求
-  :tw-1f195: [ohos_lite_http](https://gitee.com/openneusoft/ohos_lite_http) - 只需一行代码就可以发出HTTP请求！它可以将java模型转换为参数，并智能地将响应JSON命名为java模型
-  :tw-1f195: [ohos-upload-service](https://gitee.com/hihopeorg/ohos-upload-service) - 在带有进度通知的后台轻松上传文件。支持持久上传请求、自定义和自定义插件。
-  :tw-1f195: [volley](https://gitee.com/openharmony-tpc/volley) - 轻量级网络请求

[返回目录](#目录)

### 文件数据类

#### 数据库

- [greenDAO](https://gitee.com/openharmony-tpc/greenDAO) - 最常用的数据库组件
- [Activeohos](https://gitee.com/openharmony-tpc/Activeohos) - 数据库sqlite封装
- [RushOrm](https://gitee.com/openharmony-tpc/RushOrm) - 通过将Java类映射到SQL表来替代对SQL的需求，封装为易于操作的数据库
- [LitePal](https://gitee.com/openharmony-tpc/LitePal) - 数据库sqlite封装，简化sqlite操作
- [debug-database](https://gitee.com/baijuncheng-open-source/debug-database) - 封装原生数据库的增删改查操作， ORM方式操作对象对应数据库中的数据
- [ohos-database-sqlcipher](https://gitee.com/openharmony-tpc/ohos-database-sqlcipher) - 数据库加密
- [ohos-NoSql](https://gitee.com/openharmony-tpc/ohos-NoSql) - 轻量数据库
- [ormlite-ohos](https://gitee.com/openharmony-tpc/ormlite-ohos) - 数据库
-  :tw-1f195: [nitrite-java](https://gitee.com/openneusoft/nitrite-java) - Java嵌入式nosql文档库
-  :tw-1f195: [EasiestSqlLibrary](https://gitee.com/openneusoft/easiest-sql-library) - 最简单的对数据库进行增删改查
-  :tw-1f195: [rdb-explorer](https://gitee.com/openneusoft/rdb-explorer) - 适用于鸿蒙数据库的 快速简单的查看和管理
-  :tw-1f195: [ohos_dbinspector](https://gitee.com/ts_ohos/ohos_dbinspector) - OpenHarmonyOS 实现的数据库实时显示效果应用

[返回目录](#目录)

#### Preferences

- [rx-preferences](https://gitee.com/openharmony-tpc/rx-preferences) - 以rxjava的形式来保存和获取配置文件中的参数
- [preferencebinder](https://gitee.com/openharmony-tpc/preferencebinder) - 基于Preferences封装存储工具
- [PreferenceRoom](https://gitee.com/chinasoft_ohos/PreferenceRoom) - 一个高效且结构化管理Preference的功能库
- [tray](https://gitee.com/baijuncheng-open-source/tray) - Preference 替代库
-  :tw-1f195: [Secured-Preference-Store](https://gitee.com/archermind-ti/secured-preference-store) - openharmony Preferences的包装器，使用256位AES加密对内容进行加密

[返回目录](#目录)

#### 数据存储

- [DiskLruCache](https://gitee.com/hihopeorg/DiskLruCache) - 磁盘Lru存储
- [MMKV](https://gitee.com/hihopeorg/MMKV) - 数据持久化键值对存储
- [hawk](https://gitee.com/openharmony-tpc/hawk) - 安全，简单的键值存储
- [tray](https://gitee.com/openharmony-tpc/tray) - 跨进程数据管理方法
- [Parceler](https://gitee.com/openharmony-tpc/Parceler) - 任何类型的数据传输

[返回目录](#目录)


### UI-自定义控件

#### Image

- [PhotoView](https://gitee.com/openharmony-tpc/PhotoView) - 图片缩放查看
- [CircleImageView](https://gitee.com/openharmony-tpc/CircleImageView) - 圆形图片
- [RoundedImageView](https://gitee.com/openharmony-tpc/RoundedImageView) - 圆角图片
- [subsampling-scale-image-view](https://gitee.com/openharmony-tpc/subsampling-scale-image-view) - 一个图片浏览工具，利用局部剪裁的算法支持超高清图片浏览且不卡顿，支持缩放平移等功能。
- [ContinuousScrollableImageView](https://gitee.com/openharmony-tpc/ContinuousScrollableImageView) - 带动画播放的Image
- [AvatarImageView](https://gitee.com/openharmony-tpc/AvatarImageView) - 头像显示库
-  :tw-1f195: [PhotoDraweeView](https://gitee.com/chinasoft_ohos/PhotoDraweeView) - 多场景图片缩放移动处理
-  :tw-1f195: [SuperImageView](https://gitee.com/chinasoft2_ohos/SuperImageView) - 无论图像大小如何，我们都需要在某些地方裁剪图像，支持网络图片裁剪
-  :tw-1f195: [PaletteImageView](https://gitee.com/archermind-ti/palette-image-view) - PaletteImageView是一个可以解析图片中颜色，同时还可以为图片设置多彩阴影的控件
-  :tw-1f195: [path-view](https://gitee.com/archermind-ti/path-view) - 读取web版svg文件（根节点为svg）并通过path measure对path路径加载设置动画
-  :tw-1f195: [collageview](https://gitee.com/openneusoft/collageview) - 用于在应用程序中创建简单的照片拼贴。例如，在个人资料页
-  :tw-1f195: [EffectiveShapeView](https://gitee.com/baijuncheng-open-source/effective-shape-view) - 一个根据输入的数值，绘制多边形，更改多边形边界宽度，设置附着三角形位置的库
-  :tw-1f195: [BlurImageView](https://gitee.com/openharmony-tpc/BlurImageView) - 高斯模糊图片
-  :tw-1f195: [NineGridImageView](https://gitee.com/openharmony-tpc/NineGridImageView) - 九宫格图片展示

[返回目录](#目录)

#### Text

- [drawee-text-view](https://gitee.com/openharmony-tpc/drawee-text-view) - 富文本组件
- [ReadMoreTextView](https://gitee.com/openharmony-tpc/ReadMoreTextView) - 点击展开的Text控件
- [MaterialEditText](https://gitee.com/openharmony-tpc/MaterialEditText) - 基于MaterialDesign设计的自定义输入框，可以支持多种风格，不同样式颜色的设置。并且拥有验证判断等功能同时支持正则计算
- [XEditText](https://gitee.com/openharmony-tpc/XEditText) - 自定义特殊效果输入
- [lygttpod_SuperTextView](https://gitee.com/hihopeorg/lygttpod_SuperTextView) - 各种样式的自定义Text控件
- [TagView](https://gitee.com/chinasoft_ohos/TagView) - 实现文本可操作标签
- [BankCardFormat](https://gitee.com/chinasoft_ohos/BankCardFormat) - 自定义银行卡号输入框
- [AutoVerticalTextview](https://gitee.com/chinasoft_ohos/AutoVerticalTextview) - 纵向自动滚动的text
- [RTextView](https://gitee.com/chinasoft_ohos/RTextView) - 自定义Text控件，支持多种形状效果
- [JustifiedTextView](https://gitee.com/chinasoft_ohos/JustifiedTextView) - 文本对齐的Text控件
- [TextBannerView](https://gitee.com/openharmony-tpc/TextBannerView) - 文字轮播图
- [ohos-viewbadger](https://gitee.com/openharmony-tpc/ohos-viewbadger) - 文本标签View
- [ticker](https://gitee.com/openharmony-tpc/ticker) - 显示滚动文本
- [stefanjauker_BadgeView](https://gitee.com/openharmony-tpc/stefanjauker_BadgeView) - 仿iOS Springboard
- [CountAnimationTextView](https://gitee.com/openharmony-tpc/CountAnimationTextView) - Text动画计数
-  :tw-1f195: [ExpandableTextView](https://gitee.com/chinasoft_ohos/ExpandableTextView) - 实现可以展开/折叠的Text控件
-  :tw-1f195: [Pinview](https://gitee.com/chinasoft_ohos/Pinview) - TextField光标设置图片背景未实现
-  :tw-1f195: [ohos-materialshadowninepatch](https://gitee.com/chinasoft_ohos/ohos-materialshadowninepatch) - 实现可以给文本设置阴影效果
-  :tw-1f195: [edittext-mask](https://gitee.com/chinasoft_ohos/edittext-mask) - 一个输入框控件，支持输入内容遮罩(掩码）
-  :tw-1f195: [Badge](https://gitee.com/chinasoft2_ohos/Badge) - 个性化文字与图片tag
-  :tw-1f195: [material-icons](https://gitee.com/openneusoft/material-icons) - 这是一个自定义控件，图标是无限可伸缩的，并且可以使用阴影以及您可以在文本上执行的所有操作进行自定义

[返回目录](#目录)

#### Button

- [FloatingActionButton](https://gitee.com/openharmony-tpc/FloatingActionButton) - 悬浮button
- [circular-progress-button](https://gitee.com/openharmony-tpc/circular-progress-button) - 一个带进度条的自定义按钮，支持多种不能样式多种状态跳转
- [progressbutton](https://gitee.com/openharmony-tpc/progressbutton) - 带进度的自定义按钮
- [SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton) - 仿ios的开关按钮
- [SlideSwitch](https://gitee.com/openharmony-tpc/SlideSwitch) - 多种样式的开关按钮
- [iOS-SwitchView](https://gitee.com/chinasoft_ohos/iOS-SwitchView) - 仿ios的开关按钮
- [Highlight](https://gitee.com/openharmony-tpc/Highlight) - 指向性功能高亮
- [SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton) - 开关按钮
- [slideview](https://gitee.com/openharmony-tpc/slideview) - 自定义滑动按钮
-  :tw-1f195: [ohos-process-button](https://gitee.com/chinasoft_ohos/ohos-process-button) - 显示Button各种加载状态
-  :tw-1f195: [Fancybuttons](https://gitee.com/chinasoft_ohos/Fancybuttons) - 可制作带icon、边框的按钮
-  :tw-1f195: [StateButton](https://gitee.com/chinasoft2_ohos/StateButton) - button点击效果
-  :tw-1f195: [AwesomeSwitch](https://gitee.com/hihopeorg/AwesomeSwitch) - AwesomeSwitch替代了标准Switch，并且比标准开关组件提供了更多的自定义功能
-  :tw-1f195: [LikeButton](https://gitee.com/baijuncheng-open-source/LikeButton) - 一点类似twitter的点赞按钮
-  :tw-1f195: [SubmitButton](https://gitee.com/openharmony-tpc/SubmitButton) - 带提交动画的按钮(tpc)

[返回目录](#目录)

#### ListContainer

- [FloatingGroupExpandableListView](https://gitee.com/openharmony-tpc/FloatingGroupExpandableListView) - 自定义list组件，支持分类带标题
- [XRecyclerView](https://gitee.com/openharmony-tpc/XRecyclerView) - 基于ListContainer 一个简单的下拉刷新上来加载的控件
- [PullToZoomInListView](https://gitee.com/openharmony-tpc/PullToZoomInListView) - 顶部放大List
- [WaveSideBar](https://gitee.com/openharmony-tpc/WaveSideBar) - 类似于通讯录带字母选择的列表组件
- [SwipeActionAdapter](https://gitee.com/openharmony-tpc/SwipeActionAdapter) - list侧滑菜单
- [ToDoList](https://gitee.com/hihopeorg/ToDoList) - 支持多样性自定义化的list控件
- [SectionedRecyclerViewAdapter](https://gitee.com/hihopeorg/SectionedRecyclerViewAdapter) - 支持多样性自定义化的list控件
- [ARecyclerView](https://gitee.com/chinasoft_ohos/ARecyclerView) - 自定义listContainer控件
- [StickyHeadersib](https://gitee.com/chinasoft_ohos/StickyHeaders) - 支持列表分组标题
- [RoundedLetterView](https://gitee.com/chinasoft_ohos/RoundedLetterView) - 简单的通讯录ui库
- [AStickyHeader_ohos](https://gitee.com/isrc_ohos/asticky-header_ohos) - 分组标题栏滑动时置顶效果
- [CalendarListview](https://gitee.com/openharmony-tpc/CalendarListview) - 日历选择器
- [SlideAndDragListView](https://gitee.com/openharmony-tpc/SlideAndDragListView) - 自定义ListContaner控件, 实现左右滑动,上下拖动更换item的位置
- [pinned-section-listview](https://gitee.com/openharmony-tpc/pinned-section-listview) - 支持列表分组标题
- [HeaderAndFooterRecyclerView](https://gitee.com/openharmony-tpc/HeaderAndFooterRecyclerView) - 支持addHeaderView，addFooterView到ListContainer
- [MultiType](https://gitee.com/openharmony-tpc/MultiType) - 为简便ListContainer创建多种类型
- [StickyListHeaders](https://gitee.com/openharmony-tpc/StickyListHeaders) - 支持列表分组标题
-  :tw-1f195: [MaterialSpinner](https://gitee.com/chinasoft_ohos/MaterialSpinner) - 实现Material风格的可下拉列表控件
-  :tw-1f195: [PinnedSectionItemDecoration](https://gitee.com/chinasoft_ohos/PinnedSectionItemDecoration) - 实现ListContainer滑动可悬停的标题栏
-  :tw-1f195: [RecyclerViewSwipeDismiss](https://gitee.com/chinasoft2_ohos/RecyclerViewSwipeDismiss) - 水平、垂直方向滑动删除，设置不同状态背景
-  :tw-1f195: [header-decor](https://gitee.com/chinasoft2_ohos/header-decor) - RecyclerView的粘性头部装饰器
-  :tw-1f195: [recycler-fast-scroll](https://gitee.com/chinasoft_ohos/recycler-fast-scroll) - ListContainer 的快速滑动和分区显示
-  :tw-1f195: [MaterialList](https://gitee.com/chinasoft2_ohos/MaterialList) - MaterialList是一个帮助开发者展示漂亮Card视图的功能库
-  :tw-1f195: [DragListView](https://gitee.com/chinasoft_ohos/DragListView) - 实现ListContainer多级滑动及自动居中
-  :tw-1f195: [BGASwipeItemLayout-ohos](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos) - 带弹簧效果的左右滑动控件，可作为ListContainer的item
-  :tw-1f195: [ohos-GridViewWithHeaderAndFooter](https://gitee.com/chinasoft_ohos/ohos-GridViewWithHeaderAndFooter) - 支持给网格布局的ListContainer添加头布局、尾布局
-  :tw-1f195: [Slice](https://gitee.com/chinasoft3_ohos/Slice) - 类似CardView效果的自定义控件
-  :tw-1f195: [AsymmetricGridView](https://gitee.com/chinasoft_ohos/AsymmetricGridView) - 支持跨列的网格组件
-  :tw-1f195: [ExpansionPanel](https://gitee.com/chinasoft_ohos/ExpansionPanel) - 一个效果上类似ListView的控件，支持对子控件的独立拉伸、编辑操作
-  :tw-1f195: [greedo-layout-for-ohos](https://gitee.com/chinasoft2_ohos/greedo-layout-for-ohos) - 根据图片比例展示图片流，固定高度展示图片流
-  :tw-1f195: [drag-select-recyclerview](https://gitee.com/chinasoft3_ohos/drag-select-recyclerview) - 简单的多选列表功能库
-  :tw-1f195: [SnappingSwipingRecyclerView](https://gitee.com/chinasoft2_ohos/SnappingSwipingRecyclerView) - ListContainer类似于viewpager的实现,长按即可滑动删除
-  :tw-1f195: [turn-layout-manager](https://gitee.com/chinasoft2_ohos/turn-layout-manager) - 支持四个方向切换，设置半径、偏移量、文字方向等功能
-  :tw-1f195: [OpenHarmonyTreeView](https://gitee.com/baijuncheng-open-source/OpenHarmonyTreeView) - 树状列表
-  :tw-1f195: [TreeView](https://gitee.com/baijuncheng-open-source/tree-view) - 树状列表
-  :tw-1f195: [RecycleView](https://gitee.com/baijuncheng-open-source/recycle-view) - 这是一个功能丰富而灵活的数据列表操作组件
-  :tw-1f195: [DoubleStickyHeadersList](https://gitee.com/baijuncheng-open-source/DoubleStickyHeadersList) - 一个OpenHarmony库，用于粘贴到列表顶部的双层节头。OpenHarmony小部件，特别用于显示具有多级分类的项目
-  :tw-1f195: [ohos-parallax-recyclerview](https://gitee.com/baijuncheng-open-source/ohos-parallax-recyclerview) - 一个条目滑动点击，点击按钮切换adpater,条目内容更换的库
-  :tw-1f195: [Ohos-InfiniteCards](https://gitee.com/chinasoft2_ohos/Ohos-InfiniteCards) - 叠加式卡片列表
-  :tw-1f195: [ohosSwipeLayout](https://gitee.com/openharmony-tpc/ohosSwipeLayout) - 滑动删除

[返回目录](#目录)

#### PageSlider

- [ViewPagerIndicator](https://gitee.com/openharmony-tpc/ViewPagerIndicator) - 星级最高的Slider组件
- [PageIndicatorView](https://gitee.com/openharmony-tpc/PageIndicatorView) - 自定义适配器组件
- [UltraViewPager](https://gitee.com/openharmony-tpc/UltraViewPager) - 多种样式的Slider自定义控件
- [SlidingDrawer](https://gitee.com/openharmony-tpc/SlidingDrawer) - 自定义Slider组件
- [AppIntro](https://gitee.com/openharmony-tpc/AppIntro) - 为应用程序构建一个很酷的轮播介绍
- [ParallaxViewPager](https://gitee.com/openharmony-tpc/ParallaxViewPager) - 自定义Slider组件
- [MZBannerView](https://gitee.com/openharmony-tpc/MZBannerView) - 一个简单的图片轮播控件
- [FlycoPageIndicator](https://gitee.com/openharmony-tpc/FlycoPageIndicator) - 多种样式的页面指示器
- [SCViewPager](https://gitee.com/openharmony-tpc/SCViewPager) - 具有转场动画的PageSlider自定义控件
- [imagecoverflow](https://gitee.com/openharmony-tpc/ImageCoverFlow) - 3D视角适配器
- [ohos-ConvenientBanner](https://gitee.com/hihopeorg/ohos-ConvenientBanner) - 自定义banner组件
- [Banner](https://gitee.com/chinasoft_ohos/Banner) - Banner图片轮播控件
- [Material-ViewPagerIndicator](https://gitee.com/chinasoft_ohos/Material-ViewPagerIndicator) - 页面指示器，实现平移，显隐组合动画效果
- [Banner_ohos](https://gitee.com/isrc_ohos/banner_ohos) - 广告图片轮播控件
-  :tw-1f195: [BGABanner-ohos](https://gitee.com/chinasoft_ohos/BGABanner-ohos) - 广告轮播，循环轮播
-  :tw-1f195: [ViewPagerHelper](https://gitee.com/chinasoft_ohos/ViewPagerHelper) - 能够帮你快速实现导航栏轮播图，app引导页，内置多种tab指示器，让你告别 PageSlider 的繁琐操作，专注逻辑功能
-  :tw-1f195: [VerticalViewPager](https://gitee.com/chinasoft2_ohos/VerticalViewPager) - ViewPager垂直方向页面滑动
-  :tw-1f195: [ViewPagerTransforms](https://gitee.com/chinasoft_ohos/ViewPagerTransforms) - 提供一个更易于使用和扩展PageSlide动画的实现
-  :tw-1f195: [BannerViewPager](https://gitee.com/chinasoft_ohos/BannerViewPager) - ViewPager轮播图
-  :tw-1f195: [ohos-Coverflow](https://gitee.com/chinasoft_ohos/ohos-Coverflow) - 轮播图自定义组件
-  :tw-1f195: [RollViewPager](https://gitee.com/chinasoft_ohos/RollViewPager) - 轮播图自定义组件
-  :tw-1f195: [InkPageIndicator](https://gitee.com/chinasoft_ohos/InkPageIndicator) - ViewPager指示器控件
-  :tw-1f195: [AdvancedPagerSlidingTabStrip](https://gitee.com/baijuncheng-open-source/advanced-pager-sliding-tab-strip) - AdvancedPagerSlidingTabStrip是一种HarmonyOS平台的导航控件，完美兼容HarmonyOS自带库和兼容库的PageSlider组件
-  :tw-1f195: [WoWoViewPager](https://gitee.com/baijuncheng-open-source/WoWoViewPager) - 结合ViewPager和动画
-  :tw-1f195: [HorizontalPicker](https://gitee.com/openharmony-tpc/HorizontalPicker) -一个简单的横向菜单选择器控件
-  :tw-1f195: [SmartTabLayout](https://gitee.com/hihopeorg/SmartTabLayout) - 自定义TabLayout组件
-  :tw-1f195: [StatefulLayout](https://gitee.com/openharmony-tpc/StatefulLayout) - 可以左右切换布局有点类似PageSlider，显示最常见的布局状态模板，如加载、空、错误布局等
-  :tw-1f195: [NavigationTabStrip](https://gitee.com/hihopeorg/NavigationTabStrip) - Viewpager导航指示器,提供多种样式,支持自定义
-  :tw-1f195: [FlycoTabLayout](https://gitee.com/openharmony-tpc/FlycoTabLayout) - 自定义TabLayout组件，支持三种模式多种状态设置。
-  :tw-1f195: [ViewPagerIndicator](https://gitee.com/baijuncheng-open-source/ViewPagerIndicator) - ViewPager指示器
-  :tw-1f195: [Banner-Slider](https://gitee.com/baijuncheng-open-source/banner-slider) - 一个简易的图片滑动的库

[返回目录](#目录)

#### ProgressBar

- [MaterialProgressBar](https://gitee.com/openharmony-tpc/MaterialProgressBar) - 多种样式自定义progressbar
-  :tw-1f195: [MaterialRatingBar](https://gitee.com/openharmony-tpc/MaterialRatingBar) - Material样式的Rating
- [discreteSeekBar](https://gitee.com/openharmony-tpc/discreteSeekBar) - 基于populdialog控件实现动画冒泡式显示进度的一个自定义seekbar控件
- [materialish-progress](https://gitee.com/openharmony-tpc/materialish-progress) - 一个简单的圆形进度条，支持有数值和没数值两种模式的进度设置
- [ohos-HoloCircularProgressBar](https://gitee.com/openharmony-tpc/ohos-HoloCircularProgressBar) - 自定义progressBar
- [circular-music-progressbar](https://gitee.com/openharmony-tpc/circular-music-progressbar) - 类似于音乐播放器的圆形progressbar
- [SectorProgressView](https://gitee.com/openharmony-tpc/SectorProgressView) - 自定义圆形progressBar
- [LikeSinaSportProgress](https://gitee.com/openharmony-tpc/LikeSinaSportProgress) - 仿新浪体育客户端的点赞进度条
- [ArcSeekBar](https://gitee.com/hihopeorg/ArcSeekBar) - 带有弧度的seekbar
- [MaterialishProgress](https://gitee.com/hihopeorg/materialish-progress) - Materia风格的Progress控件
- [RoundCornerProgressBar](https://gitee.com/hihopeorg/RoundCornerProgressBar) - 进度条效果设置库
- [BoxedVerticalSeekBar](https://gitee.com/chinasoft_ohos/BoxedVerticalSeekBar) - 自定义纵向seekbar
- [ProgressWheel_ohos](https://gitee.com/isrc_ohos/progress-wheel_ohos) - 开源进度轮
- [MagicProgressWidget](https://gitee.com/openharmony-tpc/MagicProgressWidget) - 颜色渐变的圆形进度条和纯色轻量横向进度条
- [NumberProgressBar](https://gitee.com/openharmony-tpc/NumberProgressBar) - 一款可显示数字，可设置进度条颜色，文字大小等属性自定义数字进度条
- [ArcProgressStackView](https://gitee.com/openharmony-tpc/ArcProgressStackView) - 弧形模式下显示进度条
- [ProgressPieView](https://gitee.com/openharmony-tpc/ProgressPieView) - 自定义进度饼
- [CoreProgress](https://gitee.com/openharmony-tpc/CoreProgress) - 上传加载进度框架
- [CircularProgressView](https://gitee.com/openharmony-tpc/CircularProgressView) - Material圆形进度条
- [ButtonProgressBar](https://gitee.com/openharmony-tpc/ButtonProgressBar) - 自定义按钮进度条
- [ProgressView](https://gitee.com/openharmony-tpc/ProgressView) - 自定义ProgressView
- [CircleProgress](https://gitee.com/openharmony-tpc/CircleProgress) - 自定义圆形进度条
- [CProgressButton](https://gitee.com/openharmony-tpc/CProgressButton) - 自定义进度条按钮
- [WhorlView](https://gitee.com/openharmony-tpc/WhorlView) - 带螺纹样式的进度条
-  :tw-1f195: [ACProgressLite](https://gitee.com/chinasoft2_ohos/ACProgressLite) - openharmony 加载控件库，简洁、易用、可定制性强。用于快速实现类似 iOS 的 “加载中” 等弹出框。
-  :tw-1f195: [IndicatorSeekBar](https://gitee.com/chinasoft_ohos/IndicatorSeekBar) - 自定义可滑动进度条库
-  :tw-1f195: [Zloading](https://gitee.com/chinasoft_ohos/Zloading) - 一款自定义的炫酷的加载动画类库
-  :tw-1f195: [AndRatingBar](https://gitee.com/chinasoft_ohos/AndRatingBar) - 继承自原生Rating，具有原生的滑动选择等特性，并且可以自定义大小，间距，颜色，图标，支持从右当左
-  :tw-1f195: [BubbleSeekBar](https://gitee.com/chinasoft2_ohos/BubbleSeekBar) - 自定义SeekBar，进度变化由可视化气泡样式呈现，定制化程度较高，适合大部分需求
-  :tw-1f195: [SeekBarCompat](https://gitee.com/hihopeorg/SeekBarCompat) - SeekBarCompat是一个Slider的封装库
-  :tw-1f195: [FABProgressCircle](https://gitee.com/baijuncheng-open-source/fabprogress-circle) - 圆形进度条
-  :tw-1f195: [TheGlowingLoader](https://gitee.com/baijuncheng-open-source/the-glowing-loader) - TheGlowingLoader组件是一个易于定制的自定义动画加载视图
-  :tw-1f195: [square-progressbar](https://gitee.com/baijuncheng-open-source/square-progressbar) - 图片边缘进度条
-  :tw-1f195: [AdhesiveLoadingView](https://gitee.com/baijuncheng-open-source/adhesive-loading-view) - 具有粘性的滑动小球，跌落反弹形成loading的效果
-  :tw-1f195: [CircleProgressBar](https://gitee.com/openharmony-tpc/CircleProgressBar) - 圆形进度条

[返回目录](#目录)

#### Dialog-弹出框

- [sweet-alert-dialog](https://gitee.com/openharmony-tpc/sweet-alert-dialog) - 一个漂亮而灵动的提醒对话框，支持succeed，error，warning等多种状态模式提示
- [LovelyDialog](https://gitee.com/openharmony-tpc/LovelyDialog) - 自定义样式的Dialog，一组简单的对话框包装类库，旨在帮助您轻松创建精美对话框
- [CookieBar](https://gitee.com/openharmony-tpc/CookieBar) - 顶部底部弹出的自定义对话框
- [Alerter](https://gitee.com/openharmony-tpc/Alerter) - 带有动画效果的顶部提示弹窗
- [StatusView](https://gitee.com/openharmony-tpc/StatusView) - 顶部弹出的状态视图
- [ohos-styled-dialogs](https://gitee.com/hihopeorg/ohos-styled-dialogs) - 自定义风格化Dialog
- [NiceDialog](https://gitee.com/chinasoft_ohos/NiceDialog) - NiceDialog基于CommonDialog的扩展，让dialog的使用更方便
- [BlurDialogFragment](https://gitee.com/baijuncheng-open-source/blur-dialog-fragment) - 模糊效果对话框
- [SnackBar_ohos](https://gitee.com/isrc_ohos/SnackBar_ohos) - 开源SnackBar消息弹框
- [michaelbel_BottomSheet](https://gitee.com/openharmony-tpc/michaelbel_BottomSheet) - material design弹框
- [search-dialog](https://gitee.com/openharmony-tpc/search-dialog) - 搜索Dialog
- [material-dialogs](https://gitee.com/openharmony-tpc/material-dialogs) - Material风格Dialog
- [BottomDialog](https://gitee.com/openharmony-tpc/BottomDialog) - 通过CommonDialog实现的底部弹窗布局，支持任意布局
- [XPopup](https://gitee.com/openharmony-tpc/XPopup) - 功能强大，交互优雅，动画丝滑的通用弹窗
-  :tw-1f195: [DialogUtil](https://gitee.com/openneusoft/dialog-util) - 各种功能样式的对话弹窗工具
-  :tw-1f195: [Hover](https://gitee.com/baijuncheng-open-source/Hover) - 一个自定义的悬浮球库
-  :tw-1f195: [StyledDialogs](https://gitee.com/baijuncheng-open-source/StyledDialogs) - 不同样式的Dialog
-  :tw-1f195: [EasyFloat](https://gitee.com/ts_ohos/easy-float) - 浮窗
-  :tw-1f195: [PowerMenu](https://gitee.com/openharmony-tpc/PowerMenu) - 实现material弹出菜单的最强大，最简单的方法。 PowerMenu可以完全自定义，并用于弹出对话框

[返回目录](#目录)

#### Layout
- [bottomNavigationF](https://gitee.com/blueskyliu/BottomNavigationF) - 这个组件提供类似flutter的scaffold小部件的解决方案解决具有生命周期的底部导航
- [vlayout](https://gitee.com/openharmony-tpc/vlayout) - 可以嵌套列表布局
- [flexbox-layout](https://gitee.com/openharmony-tpc/flexbox-layout) - 按照百分比控制的布局
- [ohosAutoLayout](https://gitee.com/openharmony-tpc/ohosAutoLayout) - 可根据设计尺寸按比例缩放的屏幕适配框架
- [yoga](https://gitee.com/openharmony-tpc/yoga) - facebook基于flexbox的布局引擎
- [TextLayoutBuilder](https://gitee.com/hihopeorg/TextLayoutBuilder) - facebook的一款textlayout组件，支持文本的创建、文本字体、大小、颜色设置等
- [FlowLayout](https://gitee.com/hihopeorg/FlowLayout) - 流式布局实现
- [ShadowLayout](https://gitee.com/chinasoft_ohos/ShadowLayout) - 带阴影效果的自定义layout
- [ExpandableLayout](https://gitee.com/chinasoft_ohos/ExpandableLayout) - 可折叠展开的layout
- [LayoutManagerGroup](https://gitee.com/openharmony-tpc/LayoutManagerGroup) - 负责测量和放置RecyclerView中的项目视图
- [Flipboard/bottomsheet](https://gitee.com/openharmony-tpc/bottomsheet) - 从屏幕底部显示可忽略的View
- [ohos-flowlayout](https://gitee.com/openharmony-tpc/ohos-flowlayout) - 流布局
- [ExpandableLayout](https://gitee.com/openharmony-tpc/ExpandableLayout) - 可动画扩展折叠子view布局
-  :tw-1f195: [CarouselLayoutManager](https://gitee.com/chinasoft_ohos/CarouselLayoutManager) - 支持点击快速定位，快速滑动，点击显示当前item的下标
-  :tw-1f195: [shadow-layout](https://gitee.com/chinasoft2_ohos/shadow-layout) - 可以设置图片以及按钮的阴影效果
-  :tw-1f195: [loadinglayout](https://gitee.com/chinasoft_ohos/loadinglayout) - 简单实用的页面多状态布局(content,loading,empty,error)
-  :tw-1f195: [material-about-library](https://gitee.com/chinasoft_ohos/material-about-library) - material-about-library库包含了多种样式的选项条，多用于“关于”页面
-  :tw-1f195: [MultiViewAdapter](https://gitee.com/chinasoft2_ohos/MultiViewAdapter) - 所有的布局用一个适配器去实现
-  :tw-1f195: [DiagonalLayout](https://gitee.com/chinasoft3_ohos/DiagonalLayout) - 利用对角线布局，实现新的设计风格
-  :tw-1f195: [ohos-card-form](https://gitee.com/chinasoft2_ohos/ohos-card-form) - 表单是一个现成的卡形式布局
-  :tw-1f195: [ohos-titlebar](https://gitee.com/chinasoft2_ohos/ohos-titlebar) - 抛弃在开发过程中，因页面过多，需要构建大量重复的标题栏布局。本项目总结了几种常用的使用场景，将标题栏封装成控件，Java代码实现，对当前主流的沉浸式提供了支持
-  :tw-1f195: [HtmlNative](https://gitee.com/hihopeorg/HtmlNative) - 使用HTML / CSS渲染ohos View，使用Lua来控制其逻辑（不是Webview）
-  :tw-1f195: [SwipeBackLayout](https://gitee.com/baijuncheng-open-source/SwipeBackLayout) - 侧滑返回上一页
-  :tw-1f195: [ToggleButtonGroup](https://gitee.com/baijuncheng-open-source/ToggleButtonGroup) - 一组简易的单选和多选按钮工具
-  :tw-1f195: [FlexLayout](https://gitee.com/baijuncheng-open-source/flexlayout) - 百分比布局
-  :tw-1f195: [KugouLayout](https://gitee.com/baijuncheng-open-source/kugou-layout) - 页面滑动控制
-  :tw-1f195: [ScalableLayout](https://gitee.com/baijuncheng-open-source/ScalableLayout) - 可拓展布局
-  :tw-1f195: [RearrangeableLayout](https://gitee.com/baijuncheng-open-source/rearrangeable-layout) - 子控件任意拖动
-  :tw-1f195: [ZoomLayout](https://gitee.com/ts_ohos/zoom-layout) - 可滑动的效果
-  :tw-1f195: [FoldableLayout](https://gitee.com/openharmony-tpc/FoldableLayout) - 3D翻转
-  :tw-1f195: [ohos-drag-FlowLayout](https://gitee.com/openharmony-tpc/ohos-drag-FlowLayout) - 可拖拽layout

[返回目录](#目录)

#### Tab-菜单切换

- [FlycoTabLayout](https://gitee.com/openharmony-tpc/FlycoTabLayout) - 自定义TabLayout组件，支持三种模式多种状态设置。
- [NavigationTabBar](https://gitee.com/openharmony-tpc/NavigationTabBar) - 各种样式TabBar合集
- [BottomBar](https://gitee.com/openharmony-tpc/BottomBar) - 自定义底部菜单栏
- [BottomNavigation](https://gitee.com/openharmony-tpc/BottomNavigation) - 支持多种样式自定义底部菜单栏，此库可帮助用户轻松使用底部导航栏（来自 google 的新模式）并允许进行大量自定义
- [ahbottomnavigation](https://gitee.com/openharmony-tpc/ahbottomnavigation) - 一个从 Material Design 中重现底部导航的库。
- [SHSegmentControl](https://gitee.com/chinasoft_ohos/SHSegmentControl) - 自定义菜单控件
- [BottomNavigationViewEx](https://gitee.com/openharmony-tpc/BottomNavigationViewEx) - 自定义底部导航栏
-  :tw-1f195: [SHSegmentControl](https://gitee.com/chinasoft_ohos/SHSegmentControl) - 分段器自定义UI组件
-  :tw-1f195: [AdvancedPagerSlidingTabStrip](https://gitee.com/chinasoft2_ohos/AdvancedPagerSlidingTabStrip) - 漂亮的自定义导航控件
-  :tw-1f195: [MaterialNavigationDrawer](https://gitee.com/chinasoft2_ohos/MaterialNavigationDrawer) - 具有材料设计风格和简化方法的导航抽屉栏
-  :tw-1f195: [ChromeLikeTabSwitcher](https://gitee.com/chinasoft3_ohos/ChromeLikeTabSwitcher) - ChromeLikeTabSwitcher是一个仿Chrome浏览器中Tab切换的库
-  :tw-1f195: [bubble-navigation](https://gitee.com/chinasoft3_ohos/bubble-navigation) - 轻量级的自定义导航栏组件
-  :tw-1f195: [FABRevealMenu](https://gitee.com/chinasoft3_ohos/FABRevealMenu) - 悬浮按钮自定义弹出菜单
-  :tw-1f195: [Floating-Navigation-View](https://gitee.com/chinasoft2_ohos/Floating-Navigation-View) - 一个简单的浮动操作按钮，显示锚定导航视图
-  :tw-1f195: [BoomMenu](https://gitee.com/openneusoft/boom-menu) - BoomMenu是一个爆炸式显示类component，可定制显示个数，位置等，可独自显示，也可以添加到component容器中(List等)使用
-  :tw-1f195: [segmented_control](https://gitee.com/baijuncheng-open-source/segmented_control) - 鸿蒙版本的分段控制器
-  :tw-1f195: [WearMenu](https://gitee.com/baijuncheng-open-source/WearMenu) - 手表的菜单
-  :tw-1f195: [PagerBottomTabStrip](https://gitee.com/openharmony-tpc/PagerBottomTabStrip) - 底部和侧边的导航栏

[返回目录](#目录)

#### Toast

- [Toasty](https://gitee.com/openharmony-tpc/Toasty) - 简单好用的Toast调用工具
- [FancyToast-ohos](https://gitee.com/openharmony-tpc/FancyToast-ohos) - Toast常用样式的简单封装
- [TastyToast](https://gitee.com/hihopeorg/TastyToast) - 自定义Toast控件
-  :tw-1f195: [StyleableToast](https://gitee.com/chinasoft_ohos/StyleableToast) - 通过代码或使用中的样式来样式化toasts

[返回目录](#目录)

#### Time-Date

- [ohos-times-square](https://gitee.com/openharmony-tpc/ohos-times-square) - 简单的日历组件
- [CountdownView](https://gitee.com/openharmony-tpc/CountdownView) - 多种效果的时间计时器
-  :tw-1f195: [MaterialDateRangePicker](https://gitee.com/baijuncheng-open-source/material-date-range-picker) - Material风格的时间选择
-  :tw-1f195: [circleTimer](https://gitee.com/baijuncheng-open-source/circle-timer) - 一个简单的带动画效果的钟表样式的倒计时器
-  :tw-1f195: [SublimePicker](https://gitee.com/baijuncheng-open-source/sublime-picker) - 用于时间选择的自定义控件：提供选择日期(年、月、日)，提供选择时间(时、分、秒)，提供可选择日期的重复选项等功能
-  :tw-1f195: [ohos-betterpickers](https://gitee.com/hihopeorg/ohos-betterpickers) - 日历、时间、市区等UI Dialog 弹框框架，提供可定制主题的日历选择器

[返回目录](#目录)

#### 其他UI-自定义控件
- [BGARefreshLayout-ohos](https://gitee.com/openharmony-tpc/BGARefreshLayout-ohos) - 基于多个场景的下拉刷新
-  :tw-1f195: [FunGameRefresh](https://gitee.com/openharmony-tpc/FunGameRefresh) - 一款可以打游戏的下拉刷新控件
- [ohos-Bootstrap](https://gitee.com/openharmony-tpc/ohos-Bootstrap) - 多种自定义控件合集
- [ohosSlidingUpPanel](https://gitee.com/openharmony-tpc/ohosSlidingUpPanel) - 底部上滑布局
- [Fragmentation](https://gitee.com/openharmony-tpc/Fragmentation) - 侧边菜单
- [triangle-view](https://gitee.com/openharmony-tpc/triangle-view) - 三角图
- [MaterialDesignLibrary](https://gitee.com/openharmony-tpc/MaterialDesignLibrary) - 一系列包含ProgressBar,CheckBox,Button等基础组件的materiaDesign风格的自定义集合框架
- [cardslib](https://gitee.com/openharmony-tpc/cardslib) - 卡片式布局库
- [Swipecards](https://gitee.com/openharmony-tpc/Swipecards) - 滑动卡片组件
- [SlideUp-ohos](https://gitee.com/openharmony-tpc/SlideUp-ohos) - 从下方滑动出来的布局控件
- [EazeGraph](https://gitee.com/openharmony-tpc/EazeGraph) - 柱状图圆形图山峰图
- [WheelView](https://gitee.com/openharmony-tpc/WheelView) - 轮盘选择
- [RulerView](https://gitee.com/openharmony-tpc/RulerView) - 卷尺控件
- [MultiCardMenu](https://gitee.com/openharmony-tpc/MultiCardMenu) - 底部弹出的自定义菜单集合
- [DividerDrawable](https://gitee.com/openharmony-tpc/DividerDrawable) - 分割线绘制
- [ProtractorView](https://gitee.com/openharmony-tpc/ProtractorView) - 量角器控件
- [ohos-ExpandIcon](https://gitee.com/openharmony-tpc/ohos-ExpandIcon) - 箭头控件
- [GestureLock](https://gitee.com/openharmony-tpc/GestureLock) - 可自定义配置的手势动画解锁的库，支持多种样式大小自由设置
- [williamchart](https://gitee.com/openharmony-tpc/williamchart) - 柱状图圆形图进度图山峰图
- [labelview](https://gitee.com/openharmony-tpc/labelview) - 自定义角标图
- [PatternLockView](https://gitee.com/openharmony-tpc/PatternLockView) - 自定义屏幕图案手势解锁控件
- [BadgeView](https://gitee.com/openharmony-tpc/BadgeView) - 图标的标签图
- [MaterialBadgeTextView](https://gitee.com/openharmony-tpc/MaterialBadgeTextView) - 自定义Text实现带有插入数字的彩色圆圈，该圆圈显示在图标的右上角，通常在IM应用程序中显示新消息或新功能的作用
- [SlantedTextView](https://gitee.com/openharmony-tpc/SlantedTextView) - 一个倾斜的text,适用于标签效果
- [TriangleLabelView](https://gitee.com/openharmony-tpc/TriangleLabelView) - 三角形角标图
- [GoodView](https://gitee.com/openharmony-tpc/GoodView) - 点赞+1效果的按钮，支持文本和图像
- [StateViews](https://gitee.com/openharmony-tpc/StateViews) - 展示加载中，加载成功，加载失败以及支持自定义状态的控件
- [WaveView](https://gitee.com/openharmony-tpc/WaveView) - 可自定义振幅，频率，颜色等属性的波浪进度条控件
- [CircleRefreshLayout](https://gitee.com/openharmony-tpc/CircleRefreshLayout) - 自定义下拉刷新组件，包含有趣的动画
- [TextDrawable](https://gitee.com/openharmony-tpc/TextDrawable) - 带有字母/文字的drawable
- [OhosMaterialViews](https://gitee.com/hihopeorg/ohos-material-views) - Material风格控件
- [baseAdapter](https://gitee.com/hihopeorg/baseAdapter) - ListView,RecyclerView,GridView适配器
- [Materialize](https://gitee.com/hihopeorg/Materialize) - Materia Design风格的主题库
- [FastAdapter](https://gitee.com/hihopeorg/FastAdapter) - 快速简化适配器
- [GestureViews](https://gitee.com/hihopeorg/GestureViews) - 带有手势控制和位置动画的ImageView和FrameLayout
- [GroupedRecyclerViewAdapter](https://gitee.com/hihopeorg/GroupedRecyclerViewAdapter) - RecyclerView适配器
- [ImmersionBar](https://gitee.com/hihopeorg/ImmersionBar) - 沉浸式状态栏导航栏实现
- [material](https://gitee.com/hihopeorg/material) - Material风格的UI控件库
- [MaterialDateTimePicker](https://gitee.com/hihopeorg/MaterialDateTimePicker) - Material风格的时间选择器
- [material-design-icons](https://gitee.com/hihopeorg/material-design-icons) - 提供material-design-icons图片资源
- [PanelSwitchHelper](https://gitee.com/hihopeorg/PanelSwitchHelper) - 输入法与面板流畅切换
- [SwipeBackLayout](https://gitee.com/hihopeorg/SwipeBackLayout) - 帮助构建带有向后滑动手势的应用程序
- [SwipeRevealLayout](https://gitee.com/hihopeorg/SwipeRevealLayout) - 上下左右滑动布局
- [EasyFlipView](https://gitee.com/hihopeorg/EasyFlipView) - 可以设定反转动画的自定义控件
- [JKeyboardPanelSwitch](https://gitee.com/hihopeorg/JKeyboardPanelSwitch) - 键盘面板冲突 布局闪动处理方案
- [MarqueeViewLibrary](https://gitee.com/hihopeorg/MarqueeViewLibrary) - 一个方便使用和扩展的跑马灯库
- [nice-spinner](https://gitee.com/hihopeorg/nice-spinner) - 简单好用的下拉框组件
- [PullZoomView](https://gitee.com/hihopeorg/PullZoomView) - 支持下拉顶部图片放大
- [WaveView](https://gitee.com/hihopeorg/WaveView) - 水波纹动画
- [search](https://gitee.com/hihopeorg/Search) - Material Design风格的搜索组件
- [ohos-hellocharts](https://gitee.com/hihopeorg/ohos-hellocharts) - 各种表格数据统计UI控件
- [TicketView](https://gitee.com/hihopeorg/TicketView) - 类似于观影二维码的票据视图
- [ohos-StepsView](https://gitee.com/hihopeorg/ohos-StepsView) - 显示步骤执行的自定义控件
- [OXChart](https://gitee.com/hihopeorg/OXChart) - 自定义图表库
- [Captcha](https://gitee.com/chinasoft_ohos/Captcha) - 图片滑块解锁控件
- [LeafChart](https://gitee.com/chinasoft_ohos/LeafChart) - 支持折现、柱状的图表库
- [MessageBubbleView](https://gitee.com/chinasoft_ohos/MessageBubbleView) - 仿QQ未读消息气泡，可拖动删除
- [SuperLike](https://gitee.com/chinasoft_ohos/SuperLike) - 表情点赞功能
- [ohos_maskable_layout](https://gitee.com/chinasoft_ohos/ohos_maskable_layout) - 自定义component遮罩动画
- [Lighter](https://gitee.com/chinasoft_ohos/Lighter) - Lighter是一个首次进入页面的按钮提示功能库
- [E-signature](https://gitee.com/archermind-ti/E-signature) - 电子签名控件，支持签名边缘裁剪,根据速度进行了插值改变宽度
- [RippleView](https://gitee.com/chinasoft_ohos/RippleView) - 点击拥有水波涟漪效果动画的控件
- [StickyScrollView](https://gitee.com/chinasoft_ohos/StickyScrollView) - 支持多种样式的ScrollView控件
- [SlidingMenu_ohos](https://gitee.com/isrc_ohos/sliding-menu_ohos) - 滑动菜单
- [Ultra-Pull-To-Refresh_ohos](https://gitee.com/isrc_ohos/ultra-pull-to-refresh_ohos) - 通用下拉刷新组件
- [MPChart_ohos](https://gitee.com/isrc_ohos/mp-chart_ohos) - 图表绘制组件
- [lock-screen](https://gitee.com/openharmony-tpc/lock-screen) - 简单漂亮的锁屏库
- [Graphview](https://gitee.com/openharmony-tpc/Graphview) - ohos图表库,用于创建可视化分析的线图和条形图
- [Gloading](https://gitee.com/openharmony-tpc/Gloading) - 将应用中全局的Loading控件与页面解耦，默认提供5种加载状态（加载中、加载失败、空数据、加载成功，无网络），支持自定义其它状态
- [TimetableView](https://gitee.com/openharmony-tpc/TimetableView) - 一款开源、完善、高效的课程表控件，支持添加广告、课程重叠自动处理、透明背景设置、空白格子点击事件处理等丰富的功能
- [ohos-shapeLoadingView](https://gitee.com/openharmony-tpc/ohos-shapeLoadingView) - 仿58同城的Loading控件和Loading弹窗
- [polygonsview](https://gitee.com/openharmony-tpc/polygonsview) - 五边形蜘蛛网百分比库
- [MultipleStatusView](https://gitee.com/openharmony-tpc/MultipleStatusView) - 一个支持多种状态的自定义View,可以方便的切换到：加载中视图、错误视图、空数据视图、网络异常视图、内容视图
- [SlideshowToolbar](https://gitee.com/openharmony-tpc/SlideshowToolbar) -一款支持状态栏联动动画效果，用于播放幻灯片图片的加载组件
- [ShowcaseView](https://gitee.com/openharmony-tpc/ShowcaseView) - 引导页
- [SlidingLayout](https://gitee.com/openharmony-tpc/SlidingLayout) - 下拉上拉弹跳的果冻效果
- [AnimatedCircleLoadingView](https://gitee.com/openharmony-tpc/AnimatedCircleLoadingView) - 确定/不确定的加载视图动画
- [SwipeBack](https://gitee.com/openharmony-tpc/SwipeBack) - 手势关闭页面
- [DiscreteSlider](https://gitee.com/openharmony-tpc/DiscreteSlider) - 自定义标签滑块
- [CustomWaterView](https://gitee.com/openharmony-tpc/CustomWaterView) - 自定义仿支付宝蚂蚁森林能量控件
- [WheelPicker](https://gitee.com/openharmony-tpc/WheelPicker) - 滚轮选择器
- [EasySwipeMenuLayout](https://gitee.com/openharmony-tpc/EasySwipeMenuLayout) - 滑动菜单库
- [floatingsearchview](https://gitee.com/openharmony-tpc/floatingsearchview) - 浮动搜索View
- [FlycoRoundView](https://gitee.com/openharmony-tpc/FlycoRoundView) - 设置圆形矩形背景
- [Ratingbar](https://gitee.com/openharmony-tpc/Ratingbar) - 自定义星级/等级
- [ohos-validation-komensky](https://gitee.com/openharmony-tpc/ohos-validation-komensky) - 使用批注验证表单中的用户输入
- [SystemBarTint](https://gitee.com/openharmony-tpc/SystemBarTint) - 将背景色应用于系统
- [Leonids](https://gitee.com/openharmony-tpc/Leonids) - 粒子效果库
- [CircleView](https://gitee.com/openharmony-tpc/CircleView) - 包含标题和副标题的圆形View
- [PercentageChartView](https://gitee.com/openharmony-tpc/PercentageChartView) - 自定义百分比ChartView
- [DatePicker](https://gitee.com/openharmony-tpc/DatePicker) - 日期选择器
- [SwipeCardView](https://gitee.com/openharmony-tpc/SwipeCardView) - 自定义滑动操作卡片
- [ValueCounter](https://gitee.com/openharmony-tpc/ValueCounter) - 自定义组件计数器
- [MyLittleCanvas](https://gitee.com/openharmony-tpc/MyLittleCanvas) - 辅助作画工具集合，并且已经预设多种自定义控件
- [DragScaleCircleView](https://gitee.com/openharmony-tpc/DragScaleCircleView) - 剪裁圆形图片的控件，支持多种自定义样式属性设置
- [CircularFillableLoaders](https://gitee.com/openharmony-tpc/CircularFillableLoaders) - 水波纹浸漫式LoadingView
- [SpinMenu](https://gitee.com/openharmony-tpc/SpinMenu) - 轮盘式菜单选择控件
- [BubbleLayout](https://gitee.com/openharmony-tpc/BubbleLayout) - 自定义气泡组件
- [ohos-slidr](https://gitee.com/openharmony-tpc/ohos-slidr) - 自定义滑动条
- [ohos-SwitchView](https://gitee.com/openharmony-tpc/ohos-SwitchView) - 自定义开关按钮
- [material-intro-screen](https://gitee.com/openharmony-tpc/material-intro-screen) - Material风格的引导页组件库
- [DraggableView](https://gitee.com/openharmony-tpc/DraggableView) - 拥有3D拖拽功能浏览图片的自定义表格控件。其中拥有2种算法，通过canvas实现3D效果。
-  :tw-1f195: [GridPasswordView](https://gitee.com/chinasoft_ohos/GridPasswordView) - 支付密码视图
-  :tw-1f195: [material-ripple](https://gitee.com/chinasoft_ohos/material-ripple) - 为组件添加点击水波纹效果，水波纹效果已经全部实现
-  :tw-1f195: [vehicle-keyboard-ohos](https://gitee.com/chinasoft_ohos/vehicle-keyboard-ohos) - 快速输入车牌号
-  :tw-1f195: [GuideView](https://gitee.com/chinasoft_ohos/GuideView) - 可添加局部高亮和动画效果的遮罩式导航页
-  :tw-1f195: [RWidgetHelper](https://gitee.com/chinasoft_ohos/RWidgetHelper) - 实现多种UI:圆角、边框、渐变、图形的角度、背景色，字体颜色、渐变、水波纹、阴影、自定义类型的单选和多选
-  :tw-1f195: [ohos-otpview-pinview](https://gitee.com/chinasoft_ohos/ohos-otpview-pinview) - 用于在身份验证时输入验证码视图
-  :tw-1f195: [WidgetCase](https://gitee.com/chinasoft2_ohos/WidgetCase) - WidgetCase是一个自定义控件库
-  :tw-1f195: [WaveLoadingView](https://gitee.com/chinasoft_ohos/WaveLoadingView) - 一个提供实时波纹加载特效的控件
-  :tw-1f195: [Doodle](https://gitee.com/chinasoft_ohos/Doodle) - 图片涂鸦，具有撤消、缩放、移动、添加文字，贴图等功能
-  :tw-1f195: [XUI](https://gitee.com/chinasoft2_ohos/XUI) - 一个简洁而又优雅的ohos原生UI框架，解放你的双手！
-  :tw-1f195: [ScrollNumber](https://gitee.com/chinasoft2_ohos/ScrollNumber) - 一个 简单、优雅、易用 的滚动数字控件
-  :tw-1f195: [WheelPicker](https://gitee.com/chinasoft_ohos/WheelPicker) - 自定义滚轮选择器
-  :tw-1f195: [ohos-expression](https://gitee.com/chinasoft_ohos/ohos-expression) - 自定义表情包的库
-  :tw-1f195: [StateView](https://gitee.com/chinasoft_ohos/StateView) - 状态视图
-  :tw-1f195: [ShadowDrawable](https://gitee.com/chinasoft_ohos/ShadowDrawable) - 带阴影效果的组件库
-  :tw-1f195: [labelview](https://gitee.com/chinasoft_ohos/labelview) - 在按钮 文字 图片上添加角标
-  :tw-1f195: [MaterialSearchBar](https://gitee.com/chinasoft_ohos/MaterialSearchBar) - 实现搜索和侧滑
-  :tw-1f195: [MaterialStepperView](https://gitee.com/chinasoft_ohos/MaterialStepperView) - 竖直样式的 Stepper 组件,未来将会加入更多的样式。你可以自定义正常/激活的圆点颜色、完成图标、动画时长、是否启用动画、线条颜色以及错误高亮颜色之类的参数
-  :tw-1f195: [SuperNova-Emoji](https://gitee.com/chinasoft_ohos/SuperNova-Emoji) - SuperNova-Emoji是一个用于实现和渲染表情符号的库
-  :tw-1f195: [ikvStockChart](https://gitee.com/chinasoft2_ohos/ikvStockChart) - ikvStockChart一个简单的openharmony图表库，支持时间线，k线，macd，kdj，rsi，boll索引和交互式手势操作，包括左右滑动刷新，缩放，突出显示
-  :tw-1f195: [Genius-ohos](https://gitee.com/chinasoft2_ohos/Genius-ohos) - 是 Material Design 控件和一些常用类库组合而成
-  :tw-1f195: [material-code-input](https://gitee.com/chinasoft2_ohos/material-code-input) - Material样式的输入框
-  :tw-1f195: [OhosTreeView](https://gitee.com/chinasoft2_ohos/ohos-tree-view) - 实现可以展开/折叠的树型菜单
-  :tw-1f195: [UIWidget](https://gitee.com/chinasoft2_ohos/UIWidget) - 一个集成UIAlertDialog、UIActionSheetDialog、UIProgressDialog、RadiusView、TitleBarView、 CollapsingTitleBarLayout、StatusViewHelper、NavigationViewHelper 等项目常用UI库
-  :tw-1f195: [mua](https://gitee.com/chinasoft2_ohos/mua) - 支持多语言 支持GFM Markdown 语法说明 工具栏，用于插入Markdown代码、图片、加粗、斜体等等 菜单操作，用于保存、重命名、删除等 文件搜索 MIT协议
-  :tw-1f195: [Codeview](https://gitee.com/chinasoft2_ohos/Codeview) - 代码块高亮显示
-  :tw-1f195: [Conductor](https://gitee.com/chinasoft_ohos/Conductor) - 基于component (而非Fraction) 的HAP框架
-  :tw-1f195: [SimpleSearchView](https://gitee.com/chinasoft_ohos/SimpleSearchView) - 一款简单的基于鸿蒙风格的搜索控件
-  :tw-1f195: [datetimepicker](https://gitee.com/chinasoft_ohos/datetimepicker) - 漂亮的时间和日期选择器控件
-  :tw-1f195: [ShapeOfView](https://gitee.com/chinasoft_ohos/ShapeOfView) - 可将子控件设为多种形状的库
-  :tw-1f195: [SingleDateAndTimePicker](https://gitee.com/chinasoft_ohos/SingleDateAndTimePicker) - 一个可以同时选择日期与时间的控件
-  :tw-1f195: [material-sheet-fab](https://gitee.com/chinasoft2_ohos/material-sheet-fab) - 实现浮动操作按钮到工作表的转换
-  :tw-1f195: [MaterialShadows](https://gitee.com/chinasoft3_ohos/MaterialShadows) - 实现阴影效果的组件
-  :tw-1f195: [CountryCodePickerProject](https://gitee.com/chinasoft3_ohos/CountryCodePickerProject) - 国家城市编码选择器
-  :tw-1f195: [Alligator](https://gitee.com/chinasoft3_ohos/Alligator) - 通过注解处理器实现一套绑定ability和fraction页面切换的三方库
-  :tw-1f195: [Ohos-Week-View](https://gitee.com/chinasoft3_ohos/Ohos-Week-View) - 用于在应用程序中显示日历（周视图或日视图），它支持自定义样式
-  :tw-1f195: [Virtualview-ohos](https://gitee.com/chinasoft2_ohos/virtualview-ohos) - 通过自定义的XML文件及对应的页面展示控件,来组成一套区别于原生系统的控件展示方式
-  :tw-1f195: [CookieBar2](https://gitee.com/chinasoft2_ohos/CookieBar2) - CookieBar2是一个底部和顶部可弹出Bar的控件，且可以自动弹回或者侧滑删除
-  :tw-1f195: [CalendarExaple](https://gitee.com/chinasoft2_ohos/CalendarExaple) - 高仿钉钉和小米的日历控件，支持快速滑动，界面缓存
-  :tw-1f195: [BGATransformersTip-ohos](https://gitee.com/chinasoft2_ohos/BGATransformersTip-ohos) - 实现浮窗展示在锚点控件的任意位置，支持配置浮窗背景色，支持配置指示箭头（是否展示、展示在浮窗的任意位置、高度、圆角、颜色）
-  :tw-1f195: [blurkit-ohos](https://gitee.com/chinasoft2_ohos/blurkit-ohos) - BlurKit是一个非常易于使用和高性能的实用程序，可渲染实时模糊效果
-  :tw-1f195: [PinView](https://gitee.com/chinasoft3_ohos/PinView) - 输入框的背景颜色的动态变化，基线的显示与隐藏，明文密文的切换
-  :tw-1f195: [tooltips](https://gitee.com/chinasoft2_ohos/tooltips) - 易于使用的ohos库，可轻松在任何视图附近添加工具提示
-  :tw-1f195: [JustWeTools](https://gitee.com/hihopeorg/JustWeTools) - JustWeTools是一个方便使用的工具集，集合了众多工具类和自定义组件
-  :tw-1f195: [HoloGraphLibrary](https://gitee.com/hihopeorg/holographlibrary) - 一款集成了绘制现状图、柱状图、饼状图的工具
-  :tw-1f195: [ColorPickerView](https://gitee.com/hihopeorg/color-picker-view) - 颜色选择器
-  :tw-1f195: [arcView](https://gitee.com/hihopeorg/arcView) - 提供一套自定义搜索框控件
-  :tw-1f195: [Simple-Calendar](https://gitee.com/hihopeorg/Simple-Calendar) - 提供事件设置，日历显示
-  :tw-1f195: [saripaar](https://gitee.com/archermind-ti/saripaar) - Saripaar 是一个简单、功能丰富且功能强大的基于规则的 openharmony UI 表单验证库
-  :tw-1f195: [material-icon-lib](https://gitee.com/archermind-ti/material-icon-lib) - 一个包含 2000 多个材料矢量图标的库，可轻松用作 PixelMap和独立控件
-  :tw-1f195: [CosmoCalendar](https://gitee.com/archermind-ti/CosmoCalendar) - 高度自定义的日历库，UI精美，支持多种模式
-  :tw-1f195: [RemoteControlView](https://gitee.com/archermind-ti/remote-control-view) - 万能遥控器
-  :tw-1f195: [cache-web-view](https://gitee.com/openneusoft/CacheWebView) - 定制实现WebView缓存，离线网站，让cache配置更加简单灵活
-  :tw-1f195: [Barber](https://gitee.com/baijuncheng-open-source/barber) - 一个自定义视图样式库。提供了一个简单的基于自定义注释@StyledAttr的style接口来定义视图样式
-  :tw-1f195: [ShadowLayout](https://gitee.com/baijuncheng-open-source/ShadowLayout) - 绘制阴影的库
-  :tw-1f195: [PatternLock](https://gitee.com/baijuncheng-open-source/PatternLock) - 一个实现 Material Design 模式的图案锁库。
-  :tw-1f195: [StatusStories](https://gitee.com/baijuncheng-open-source/statusstories) - 一个高度可定制化的故事视图
-  :tw-1f195: [CurveGraphView](https://gitee.com/baijuncheng-open-source/curve-graph-view) - CurveGraphView组件为图形视图，是一种高度可定制和高性能的自定义视图，用于渲染曲线图
-  :tw-1f195: [CountryPicker](https://gitee.com/baijuncheng-open-source/country-picker) - 国家/地区选择器
-  :tw-1f195: [PinView](https://gitee.com/baijuncheng-open-source/pin-view) - PIN 码专用输入控件，支持任意长度和输入任意数据
-  :tw-1f195: [Carbon](https://gitee.com/ts_ohos/Carbon) - 一个适用于鸿蒙的自定义组件框架，帮助快速实现各种需要的效果
-  :tw-1f195: [ohos-AdvancedWebView](https://gitee.com/hihopeorg/ohos-AdvancedWebView) - 高级的webview
-  :tw-1f195: [ohos-ui](https://gitee.com/openharmony-tpc/ohos-ui) - ui库
-  :tw-1f195: [FogView_Library](https://gitee.com/openharmony-tpc/FogView_Library) - 雾化视图
-  :tw-1f195: [ohosWheelView](https://gitee.com/openharmony-tpc/ohosWheelView) - 滚轮视图

[返回目录](#目录)

### 框架类

#### 框架类

- [TheMVP](https://gitee.com/openharmony-tpc/TheMVP) - mvp框架
- [ohos-ZBLibrary](https://gitee.com/openharmony-tpc/ohos-ZBLibrary) - MVP框架，同时附有OKhttp，glide，zxing等常用工具
- [AutoDispose](https://gitee.com/hihopeorg/AutoDispose) - 基于RxJava进行自动绑定代码流式处理
- [mosby](https://gitee.com/hihopeorg/mosby) - 开源mvi、mvp模式适配项目
-  :tw-1f195: [Hermes](https://gitee.com/chinasoft_ohos/Hermes) - 一套新颖巧妙易用的openHarmony进程间通信IPC框架
-  :tw-1f195: [MVPArt](https://gitee.com/archermind-ti/mvpart) - 含有网络层的完整框架,将 **Retrofit** 作为网络层并使用 **Dagger2** 管理所有对象,成熟强大适合新建的项目
-  :tw-1f195: [VIABUS-Architecture](https://gitee.com/archermind-ti/viabus-architecture) - ViaBus 是一款响应式架构，借助总线转发数据的请求和响应，实现ui、业务的完全解耦
-  :tw-1f195: [Clean-Contacts](https://gitee.com/archermind-ti/clean-contacts) - Clean Architecture implementation on OpenHarmony
-  :tw-1f195: [grouter](https://gitee.com/archermind-ti/grouter) - harmonyos APP页面及服务组件化框架
-  :tw-1f195: [XUpdate](https://gitee.com/openneusoft/xupdate) - 一个轻量级、高可用性的版本更新框架
-  :tw-1f195: [Component](https://gitee.com/openneusoft/component) - 一个强大、灵活的组件化框架
-  :tw-1f195: [magnet](https://gitee.com/ts_ohos/magnet_hos) - 一个适用于鸿蒙的java注解框架

[返回目录](#目录)

### 动画图形类

#### 动画

- [ohosViewAnimations](https://gitee.com/openharmony-tpc/ohosViewAnimations) - 包含旋转，缩放，平移，透明及其组合的常见动画效果的动画库集合框架
- [lottie-ohos](https://gitee.com/openharmony-tpc/lottie-ohos) - json格式的动画解析渲染库
- [confetti](https://gitee.com/openharmony-tpc/confetti) - 模仿雪花飘落的动画
- [RippleEffect](https://gitee.com/openharmony-tpc/RippleEffect) - 水波纹点击动画
- [MetaballLoading](https://gitee.com/openharmony-tpc/MetaballLoading) - 一个类似圆球进度动画效果
- [ohos-Spinkit](https://gitee.com/openharmony-tpc/ohos-Spinkit) - 多种基础动画集合
- [LoadingView](https://gitee.com/openharmony-tpc/LoadingView) - 21种简单的带有动画效果的加载控件
- [desertplaceholder](https://gitee.com/openharmony-tpc/desertplaceholder) - 沙漠风格的动画占位页
- [Sequent](https://gitee.com/openharmony-tpc/Sequent) - 为一个页面中的所有子控件提供动画效果，使页面更生动
- [ohos-Views](https://gitee.com/openharmony-tpc/ohos-Views) - 包含粒子效果，脉冲button效果，progress效果，底部导航栏等自定义组件的集合
- [BezierMaker](https://gitee.com/openharmony-tpc/BezierMaker) - 简单的贝赛尔曲线绘制
- [ohos-transition](https://gitee.com/hihopeorg/ohos-transition) - 平移动画库
- [Konfetti](https://gitee.com/hihopeorg/Konfetti) - 纸屑粒子效果动画
- [LoadingDrawable](https://gitee.com/hihopeorg/LoadingDrawable) - 提供16种加载动画， 适用于下拉刷新、图片加载的占位符、以及其他耗时操作场景
- [recyclerview-animators](https://gitee.com/hihopeorg/recyclerview-animators) - 实现Item增加和删除的动画效果
- [ViewAnimator](https://gitee.com/hihopeorg/ViewAnimator) - 多种布局的动画集合
- [Ohos-spruce](https://gitee.com/hihopeorg/ohos-spruce) - 轻量级平移转场动画
- [CanAnimation](https://gitee.com/chinasoft_ohos/CanAnimation) - 使用ohos的属性动画写的一个库，可组建动画队列，可实现同时、顺序、重复播放等
- [EasingInterpolator](https://gitee.com/archermind-ti/EasingInterpolator) - 多种动画插值器轨迹展示
- [ohos-svprogress-hud-master](https://gitee.com/baijuncheng-open-source/ohos-svprogress-hud-master) - 一个精仿ios提示的弹窗提示库，包括加载动画，失败与成功提示等
- [circular-anim](https://gitee.com/baijuncheng-open-source/circular-anim) - 圆形转场动画
- [AnimatorValueLoadingIndicatorView_ohos](https://gitee.com/isrc_ohos/avloading-indicator-view_ohos) - 支持加载动画的开关和隐藏，支持多种加载动画效果
- [AZExplosion](https://gitee.com/isrc_ohos/azexplosion_ohos) - 粒子破碎效果
- [SwipeCaptcha_ohos](https://gitee.com/isrc_ohos/swipe-captcha_ohos) - 滑动验证码
- [DanmakuFlameMaster_ohos](https://gitee.com/isrc_ohos/danmaku-flame-master_ohos) - 弹幕解析绘制
- [Transitions-Everywhere](https://gitee.com/openharmony-tpc/Transitions-Everywhere) - 转场动画
- [AnimationEasingFunctions](https://gitee.com/openharmony-tpc/AnimationEasingFunctions) - 多种估值器动画运动轨迹的集合
- [MultiWaveHeader](https://gitee.com/openharmony-tpc/MultiWaveHeader) - 自定义水波控件
- [ohos-animated-menu-items](https://gitee.com/openharmony-tpc/ohos-animated-menu-items) - 自定义动画菜单条目小控件
-  :tw-1f195: [OhosCarrouselLayout](https://gitee.com/chinasoft2_ohos/OhosCarrouselLayout) - 旋转木马3D版
-  :tw-1f195: [SimpleFingerGestures_Ohos_Library](https://gitee.com/chinasoft2_ohos/simple-finger-gestures-ohos-library) - 一个可轻松实现简单的1或2个或多个手指手势的openharmony库
-  :tw-1f195: [WaveLineView](https://gitee.com/chinasoft_ohos/WaveLineView) - 一款性能内存友好的录音波浪动画
-  :tw-1f195: [DynamicGrid](https://gitee.com/chinasoft2_ohos/DynamicGrid) - 图标拖拽排序组件
-  :tw-1f195: [OhosLoadingAnimation](https://gitee.com/chinasoft2_ohos/ohos-loading-animation) - 实现多种动画加载效果
-  :tw-1f195: [BGABadgeView-ohos](https://gitee.com/chinasoft2_ohos/BGABadgeView-ohos) - 实现消息徽章拖拽出范围后爆炸效果
-  :tw-1f195: [mkloader](https://gitee.com/chinasoft2_ohos/mkloader) - 多个自定义加载动画组件
-  :tw-1f195: [SpringView](https://gitee.com/hihopeorg/SpringView) - 提供了上下拖拽刷新控件的功能组件，能够自定义下拉\上拉动画效果
-  :tw-1f195: [Swipecards](https://gitee.com/archermind-ti/swipecards) - 类似探探，自定义卡片左右滑动删除
-  :tw-1f195: [MaterialPlayPauseDrawble](https://gitee.com/archermind-ti/materialplaypausedrawable) - 带动画的点击控件，可切换播放暂停状态
-  :tw-1f195: [ENViews](https://gitee.com/openharmony-tpc/ENViews) - 各种加载动画

[返回目录](#目录)

#### 图片处理

- [SimpleCropView](https://gitee.com/openharmony-tpc/SimpleCropView) - 适用于ohos的图像裁剪库，简化了裁剪的代码，并提供了易于自定义的UI
- [Luban](https://gitee.com/openharmony-tpc/Luban) - 图片压缩工具
- [TakePhoto](https://gitee.com/openharmony-tpc/TakePhoto) - 拍照图片旋转剪裁
- [Compressor](https://gitee.com/openharmony-tpc/Compressor) - 一个轻量级且功能强大的图像压缩库。通过Compressor，您可以将大照片压缩为较小尺寸的照片，而图像质量的损失则很小或可以忽略不计，不支持WebP
- [PloyFun](https://gitee.com/openharmony-tpc/PloyFun) - 用来生成三角玻璃图片工具
- [CompressHelper](https://gitee.com/openharmony-tpc/CompressHelper) - 图片压缩，压缩Pixelmap，主要通过尺寸压缩和质量压缩，以达到清晰度最优
- [SimpleCropView](https://gitee.com/hihopeorg/SimpleCropView) - 图片裁剪工具
- [cropper](https://gitee.com/hihopeorg/cropper) - 图像裁剪工具
- [cropper2](https://gitee.com/openharmony-tpc/cropper) - 图片裁剪
- [boxing](https://gitee.com/hihopeorg/boxing) - 支持图片旋转裁剪多图选择等功能
- [Ohos-stackblur](https://gitee.com/hihopeorg/ohos-stackblur) - 图片模糊效果
- [ImageCropper_ohos](https://gitee.com/isrc_ohos/image-cropper_ohos) - 图片裁剪
- [uCrop_ohos](https://gitee.com/isrc_ohos/u-crop_ohos) - 图像裁剪
- [ Crop_ohos](https://gitee.com/isrc_ohos/crop_ohos) - 图片裁剪
- [crop_image_layout_ohos](https://gitee.com/isrc_ohos/crop_image_layout_ohos) - 图片裁剪
- [Lichenwei-Dev_ImagePicker](https://gitee.com/openharmony-tpc/Lichenwei-Dev_ImagePicker) - 图片选择预览加载器
-  :tw-1f195: [wallpaperboard](https://gitee.com/chinasoft2_ohos/wallpaperboard) - 可查看、下载、设置壁纸和锁屏的库
-  :tw-1f195: [Image-Steganography-Library-ohos](https://gitee.com/chinasoft3_ohos/Image-Steganography-Library-ohos) - 使用LSB将加密信息编码嵌入到图片中，实现隐写
-  :tw-1f195: [photo-editor-ohos](https://gitee.com/chinasoft_ohos/photo-editor-ohos) - 易于操作图片文件的oho库
-  :tw-1f195: [Ohos-CutOut](https://gitee.com/chinasoft3_ohos/Ohos-CutOut) - 对图片进行裁剪,旋转,涂鸦,渲染等效果
-  :tw-1f195: [ShadowImageView](https://gitee.com/chinasoft3_ohos/ShadowImageView) - 设置图片、设置图片半径、设置图片阴影颜色、根据图片内容获取阴影颜色
-  :tw-1f195: [touch-gallery](https://gitee.com/archermind-ti/touch-gallery) - 库用于图片浏览, 基于PageSlider，实现图片的切换、缩放、拖拽等
-  :tw-1f195: [SiliCompressor](https://gitee.com/ts_ohos/silicompressor-for-ohos) - 图片压缩
-  :tw-1f195: [ohossvg](https://gitee.com/ts_ohos/ohossvg) - svg图片

[返回目录](#目录)

### 音视频

- [jcodec java](https://gitee.com/openharmony-tpc/jcodec) - 纯java实现的音视频编解码器的库
- [VideoCache_ohos](https://gitee.com/isrc_ohos/video-cache_ohos) - 开源视频缓存项目，支持自动缓存视频并在断网状态下播放视频
- [soundtouch](https://gitee.com/openharmony-tpc/soundtouch) - 开源音频处理库，可更改音频流或音频文件的速度、音高和播放速率
- [ohosMP3Recorder](https://gitee.com/openharmony-tpc/ohosMP3Recorder) - 提供MP3录音功能
- [ijkplayer](https://gitee.com/openharmony-tpc/ijkplayer) - 基于FFmpeg的ohos视频播放器，除了常规的播放器功能外，多用于直播流场景，支持常见的各种流媒体协议和音视频格式
-  :tw-1f195: [YcVideoPlayer](https://gitee.com/chinasoft3_ohos/YcVideoPlayer) - 基础封装视频播放器player，使用简单，代码拓展性强，封装性好，主要是和业务彻底解耦，暴露接口监听给开发者处理业务具体逻辑
-  :tw-1f195: [ohos-AudioRecorder](https://gitee.com/chinasoft3_ohos/ohos-AudioRecorder) - 主要实现录音功能、暂停，播放。根据声音大小振幅有水波纹冒泡效果
-  :tw-1f195: [ChatVoicePlayer](https://gitee.com/chinasoft2_ohos/ChatVoicePlayer) - 简单音乐播放器功能，实现播放、暂停功能
-  :tw-1f195: [speechutils](https://gitee.com/chinasoft2_ohos/speechutils) - 语音转文字，文字转语音库
-  :tw-1f195: [youtube-jextractor](https://gitee.com/chinasoft2_ohos/youtube-jextractor) - 从任何youtube视频中提取视频和音频以及其他一些数据，例如视频标题，说明，作者，缩略图等
-  :tw-1f195: [audio-visualizer-ohos](https://gitee.com/chinasoft3_ohos/audio-visualizer-ohos) - 音频播放及背景联动
-  :tw-1f195: [ohos-audio-visualizer](https://gitee.com/chinasoft3_ohos/ohos-audio-visualizer) - 实现音频可视化
-  :tw-1f195: [auto-play-video](https://gitee.com/archermind-ti/auto-play-video) - 轻松实现带有视频的ListContainer
-  :tw-1f195: [fenster](https://gitee.com/ts_ohos/fenster) - 1.简易视频播放器功能 支持暂停和播放，播放进度显示，快进和快退功能 2.标准播放器功能 支持 暂停播放，播放进度显示，快进和快退，音量调节，亮度调节等功能 3.视频缩放 支持不同size的缩放 4.开发者可以扩展Next和Pre键，实现自己想要的功能
-  :tw-1f195: [VideoPlayerManager](https://gitee.com/ts_ohos/VideoPlayerManager) - openharmony实现的VideoPlayerManager功能
-  :tw-1f195: [RxOhosAudio](https://gitee.com/ts_ohos/RxOhosAudio) - 音频的录制和播放
-  :tw-1f195: [mp4parser](https://gitee.com/ts_ohos/mp4parser_hos) - 用于读取、写入和创建MP4容器的JavaAPI。操纵容器不同于对视频和音频进行编码和解码。openharmony移植组件
-  :tw-1f195: [QSVideoPlayer](https://gitee.com/ts_ohos/qsvideo-player_hos) - 支持设置视频比例，支持两种悬浮窗，支持拓展解码器，支持本地缓存，支持倍速静音等，只需100行不到的java代码即可打造自己的播放器，提供DemoQSVideoView成品播放器,支持手势,清晰度，一句代码集成弹幕。openharmony移植组件
-  :tw-1f195: [lingorecorder](https://gitee.com/ts_ohos/lingorecorder-for-ohos) - 音频处理

[返回目录](#目录)



### 游戏

-  :tw-1f195: [JustWeEngine](https://gitee.com/openneusoft/just-we-engine) - 原生游戏框架，可以基于这个框架开发一些简单的小游戏，比如打飞机，骨骼精灵打怪等

[返回目录](#目录) 